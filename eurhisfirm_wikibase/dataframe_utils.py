import datetime
import logging
import re
from pathlib import Path
from typing import List, Optional, Set

import numpy as np
import pandas as pd

log = logging.getLogger(__name__)

# Replace non significant values
NON_SIGNIFICANT_DATES = [
    # (column name, value to replace, new value)
    ("STARTDATE", datetime.date(1000, 1, 1), None),
    ("ENDDATE", datetime.date(3999, 12, 31), None),
    ("IN_YEARBOOK_FROM", 1000, 0),
    ("IN_YEARBOOK_TO", 1000, 0),
    ("IN_YEARBOOK_TO", 3999, 0),
]

NORM_SPACES_RE = re.compile(r"\s+")


def normalize_space(str_value):
    """Normalize spaces of the given string.

    This function is named as XSLT function 'normalize-space'.
    See https://developer.mozilla.org/fr/docs/Web/XPath/Fonctions/normalize-space
    Used to ensure that string sent to wikibase are safe.
    """
    return NORM_SPACES_RE.sub(" ", str_value).strip()


def int_to_str_series(v: float) -> Optional[str]:
    """used in pandas df.apply() to convert int column into string column.

    nan values are converted into None values.
    """
    return None if np.isnan(v) else str(int(v))


def load_dataframe(
    file_path: Path,
    index_col: str,
    date_columns: List[str] = None,
    sort_columns: List[str] = None,
    text_columns: Set[str] = None,
) -> pd.DataFrame:
    """Load pandas data frame from CSV."""

    if not file_path.exists():
        log.error("CSV file not found [%r]", file_path)
        raise ValueError(f"CSV file not found: {file_path!r}")

    # Date parsing
    def parse_french_date(dates: List[str]):
        if isinstance(dates, float):
            return None
        return [
            datetime.datetime.strptime(d, "%d/%m/%Y").date()
            if isinstance(d, str)
            else None
            for d in dates
        ]

    options = {}
    if date_columns:
        options["parse_dates"] = date_columns
        options["date_parser"] = parse_french_date

    df = pd.read_csv(file_path, index_col=index_col, **options)

    # Post  processing
    if sort_columns:
        df = df.sort_values(by=sort_columns)

    if text_columns:
        for text_column in text_columns:
            df[text_column] = df[text_column].apply(str).apply(normalize_space)

    # Ignore null dates
    for col_name, value, new_value in NON_SIGNIFICANT_DATES:
        if col_name in df.columns:
            df.loc[df[col_name] == value, col_name] = new_value

    return df.replace({"nan": None})


def empty_string(value: str) -> bool:
    return value is None or len(value) == 0
