from pathlib import Path
from typing import Callable, Dict, List

from wikidataintegrator.wdi_core import WDItemEngine

WIKIBASE_LABEL_MAX_LEN = 250
WIKIBASE_STRING_MAX_LEN = 400


def truncate_label(str_value: str):
    """Truncate label to maximum length accepted by wikibase."""
    return truncate(str_value, WIKIBASE_LABEL_MAX_LEN)


def truncate(str_value: str, maxlen: int) -> str:
    if len(str_value) <= maxlen:
        return str_value
    return str_value[: maxlen - 6].rstrip() + " [...]"


def itemid_from_url(item_url):
    """Convert http://wikibase.svc/entity/Q17 into Q17."""
    return Path(item_url).name if item_url else None


def create_entity(
    login_instance,
    item_engine,
    label_dict: Dict[str, str],
    description_dict: Dict[str, str],
    aliases_dict: Dict[str, List[str]],
    entity_type: str,
    datatype: str = None,
) -> str:
    """Create a new Wikibase item of property.

    This is a helper function, maybe missing in WikidataIntegrator.
    """

    # Basic checks
    if entity_type not in ("item", "property"):
        raise ValueError(f"Invalid entity type [{entity_type}]")
    if entity_type == "property" and datatype is None:
        raise ValueError("Missing datatype info to create property")

    entity = item_engine(new_item=True)

    # Set all labels
    for lang, label in label_dict.items():
        entity.set_label(truncate_label(label), lang=lang)

    # Set all descriptions
    if description_dict:
        for lang, description in description_dict.items():
            entity.set_description(description, lang=lang)

    # Set all aliases
    if aliases_dict:
        for lang, aliases in aliases_dict.items():
            entity.set_aliases(aliases, lang=lang)

    options = {"property_datatype": datatype} if entity_type == "property" else {}

    entity.write(login_instance, entity_type=entity_type, **options)
    return entity.wd_item_id


def iter_info_item_id_from_wikibase(
    item_engine: WDItemEngine,
    sparql_endpoint_url: str,
    sparql_query: str,
    value_func: Callable,
):
    """Help itering over (value, item_id) tuples.

    Please, use '?item' parameter in sparql query.
    """
    results = item_engine.execute_sparql_query(
        sparql_query, endpoint=sparql_endpoint_url
    )

    if "results" in results:
        for result in results["results"].get("bindings", []):
            item_id = itemid_from_url(result["item"]["value"])
            result_map = {
                name: result[name]["value"] for name in result if name != "item"
            }
            yield (value_func(result_map), item_id)
