import datetime
import logging
from typing import Any, List

from wikidataintegrator.wdi_core import WDItemID, WDTime

from base_entities_gen import BaseProperty

from .wdi_utils import format_wikibase_date

log = logging.getLogger(__name__)


def database_references(
    database_entity_id: str, additional_references: List[Any] = None
):
    """Build source database reference."""
    if additional_references is None:
        additional_references = []
    reference = [
        WDItemID(database_entity_id, BaseProperty.SOURCE_DATABASE, is_reference=True),
        WDTime(
            format_wikibase_date(datetime.date.today()),
            BaseProperty.RETRIEVED,
            is_reference=True,
        ),
        *additional_references,
    ]
    return [reference]


def date_qualifiers(
    row,
    none_if_empty: bool = False,
    is_qualifier: bool = True,
    references=None,
    start_date_property_id: str = BaseProperty.START_DATE,
    end_date_property_id: str = BaseProperty.END_DATE,
):
    """Build date qualifier list, return None if both start_date and end_date is empty."""

    qualifier_list = []
    if row.STARTDATE:
        qualifier_list.append(
            WDTime(
                format_wikibase_date(row.STARTDATE),
                start_date_property_id,
                is_qualifier=is_qualifier,
                references=references,
            )
        )
    if row.ENDDATE:
        qualifier_list.append(
            WDTime(
                format_wikibase_date(row.ENDDATE),
                end_date_property_id,
                is_qualifier=is_qualifier,
                references=references,
            )
        )

    if qualifier_list:
        return qualifier_list
    return None if none_if_empty else []
