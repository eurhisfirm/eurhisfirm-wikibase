import concurrent.futures
import datetime
import logging
from dataclasses import dataclass
from typing import Any, Callable, Dict, List, Optional, Tuple

import pandas as pd
from wikidataintegrator.wdi_core import WDApiError, WDItemEngine, WDString
from wikidataintegrator.wdi_helpers import id_mapper
from wikidataintegrator.wdi_login import WDLogin

from .wikibase_utils import (
    WIKIBASE_STRING_MAX_LEN,
    iter_info_item_id_from_wikibase,
    truncate,
    truncate_label,
)

log = logging.getLogger(__name__)


def format_wikibase_date(d: datetime.date) -> str:
    """Format dataframe date into wikibase date."""
    d = datetime.datetime.combine(d, datetime.datetime.min.time())
    d_str = d.isoformat() + "Z"
    if not (d_str.startswith("+") or d_str.startswith("-")):
        d_str = "+" + d_str
    return d_str


def SafeWDString(value: str, prop_nr: str, **kwargs):
    """Create a new WikidataString, truncating value if len exceeds wikibase string length limit."""
    if type(value) != str:
        log.error("BAD VALUE TYPE [%s] [%r] [%s]", type(value), value, prop_nr)

    return WDString(truncate(value, WIKIBASE_STRING_MAX_LEN), prop_nr, **kwargs)


def compute_item(
    item_engine: WDItemEngine,
    statements,
    item_label: str,
    item_id: str = None,
    item_label_lang: Any = None,  # can be either a language (str) or a language list (List[str])
    fast_run_base_filter: Dict[str, str] = None,
):
    """Factor code to compute a new (or to update) item."""

    if item_label_lang is None:
        raise ValueError("No lang defined in compute_item")

    options = {
        "data": statements,
    }
    if fast_run_base_filter:
        options["fast_run"] = True
        options["fast_run_base_filter"] = fast_run_base_filter

    item = (
        item_engine(item_id, **options)
        if item_id is not None
        else item_engine(new_item=True, **options)
    )

    # Set item label(s)
    lang_list = [item_label_lang] if type(item_label_lang) == str else item_label_lang
    for lang in lang_list:
        item.set_label(truncate_label(item_label), lang=lang)
    return item


def write_item(
    item: WDItemEngine,
    login_instance: WDLogin,
    value: Any,
    item_type_label: str,
    check_if_necessary: bool = True,
    label: Optional[str] = None,
) -> Optional[str]:

    if check_if_necessary and not item.require_write:
        item_id = item.wd_item_id
        log.info(
            "Skip unchanged item %s corresponding to %s",
            item_id,
            value,
        )
        return None

    info = f"{value}: {label}" if label else value

    try:
        item_id = item.write(login_instance)
        log.debug("New %s item %s: « %r »", item_type_label, item_id, info)
        return item_id

    except WDApiError as exc:
        log.error(
            "Error creating %s item corresponding to « %s »: %r",
            item_type_label,
            info,
            exc,
        )

    return None


def find_latest_df_name(names_df: pd.DataFrame, column_name: str = "NAME"):
    return names_df.iloc[-1][column_name]


class WBHandle:
    """Store wikibase config and provide handy methods."""

    def __init__(
        self,
        wikibase_api_url: str,
        mediawiki_bot_username: str,
        mediawiki_bot_password: str,
        sparql_endpoint_url: str,
        default_language: str,
        available_languages_str: str,
    ):
        self.mediawiki_api_url = wikibase_api_url
        self.login_instance = WDLogin(
            mediawiki_api_url=wikibase_api_url,
            user=mediawiki_bot_username,
            pwd=mediawiki_bot_password,
        )

        self.item_engine = WDItemEngine.wikibase_item_engine_factory(
            mediawiki_api_url=wikibase_api_url,
            sparql_endpoint_url=sparql_endpoint_url,
        )
        self.sparql_endpoint_url = sparql_endpoint_url

        self.default_language = default_language

        if available_languages_str is None:
            available_languages_str = ""
        self.language_list = [
            lang.strip() for lang in available_languages_str.split(",") if lang
        ]

    def iter_info_item_id_from_wikibase(self, sparql_query: str, value_func: Callable):
        """Help itering over (value, item_id) tuples.

        Please, use '?item' parameter in sparql query.
        """

        return iter_info_item_id_from_wikibase(
            self.item_engine, self.sparql_endpoint_url, sparql_query, value_func
        )

    def write_item(
        self,
        item: WDItemEngine,
        value: Any,
        item_type_label: str,
        check_if_necessary: bool = True,
        label: Optional[str] = None,
    ) -> Optional[str]:

        return write_item(
            item,
            self.login_instance,
            value,
            item_type_label,
            check_if_necessary,
            label,
        )

    def compute_item(
        self,
        statements,
        item_label: str,
        item_id: str = None,
        item_label_lang: Any = None,  # can be either a language (str) or a language list (List[str])
        fast_run_base_filter: Dict[str, str] = None,
    ):
        if item_label_lang is None:
            item_label_lang = self.language_list
        return compute_item(
            self.item_engine,
            statements,
            item_label,
            item_id,
            item_label_lang,
            fast_run_base_filter,
        )

    def build_multilang_item(
        self,
        statements,
        multilang_labels: List[Tuple[str, str]],
        multilang_aliases: List[Tuple[List[str], str]] = None,
        multilang_descriptions: List[Tuple[str, str]] = None,
        fast_run_base_filter: Dict[str, str] = None,
    ):
        """Build multilang item."""
        options = {
            "data": statements,
        }
        if fast_run_base_filter:
            options["fast_run"] = True
            options["fast_run_base_filter"] = fast_run_base_filter

        item = self.item_engine(new_item=True, **options)

        # Set label(s)
        if multilang_labels:
            for label, lang in multilang_labels:
                item.set_label(truncate_label(label), lang=lang)

        # Set description(s)
        if multilang_descriptions:
            for description, lang in multilang_descriptions:
                item.set_description(description, lang=lang)

        # Set alias(es)
        if multilang_aliases:
            for aliases, lang in multilang_aliases:
                item.set_aliases(aliases, lang=lang)

        return item

    def id_mapper(self, entity_id: str):
        return id_mapper(entity_id, endpoint=self.sparql_endpoint_url)


@dataclass
class Context:
    max_workers: int
    wb_handle: WBHandle


def format_duration(nb_of_seconds: int) -> str:
    """Compute duration string from given nb of seconds.

    >>> format_duration(4)
    "4s"
    >>> format_duration(100)
    "1m40s"
    >>> format_duration(180)
    "3m"
    >>> format_duration(4000)
    "1h6m40s"
    """
    duration = ""
    h, s = divmod(nb_of_seconds, 3600)
    if h:
        duration += f"{h}h"
    m, s = divmod(s, 60)
    if m:
        duration += f"{m}m"
    if s:
        duration += f"{s}s"
    return duration if duration else "no time"


def format_speed(count: int, elapsed_in_seconds: int) -> str:
    if elapsed_in_seconds == 0:
        return ""
    return f" ({count / elapsed_in_seconds:.1f} items/s)"


def parallel_item_process(process_func: Callable, info_list: List, max_workers=None):

    # nothing to create, stop here
    count = len(info_list)
    if count == 0:
        log.info("no items to process")
        return []

    # else get time to compute elapsed time
    start = datetime.datetime.now()
    log.info("%d item(s) to process", count)

    # run parallel tasks
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:

        # do the job
        result_list = executor.map(process_func, info_list)

    # how much time did it take?
    elapsed = datetime.datetime.now() - start
    elapsed_in_seconds = round(elapsed.total_seconds())
    log.info(
        "done in %s%s",
        format_duration(elapsed_in_seconds),
        format_speed(count, elapsed_in_seconds),
    )

    return result_list
