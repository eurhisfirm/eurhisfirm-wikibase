import csv
import logging
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Tuple

import pandas as pd
from wikidataintegrator.wdi_core import WDExternalID, WDItemID

from base_entities_gen import BaseItem, BaseProperty

from .common_import import database_references, date_qualifiers
from .dataframe_utils import empty_string, load_dataframe
from .wdi_utils import (
    Context,
    SafeWDString,
    WBHandle,
    find_latest_df_name,
    parallel_item_process,
)
from .wikibase_utils import itemid_from_url

PROJECT_DIR = Path(__file__).parent.parent

log = logging.getLogger(__name__)


def load_corporations(file_path: Path) -> pd.DataFrame:
    return load_dataframe(file_path, "ID", date_columns=["STARTDATE", "ENDDATE"])


def load_corporation_names(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"NAME"},
    )


def load_corporation_locations(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"ADDRESS", "CITY", "COUNTRY"},
    )


def load_corporation_legal_status(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"JURIDISCH_STATUUT", "COMMENTS", "SOURCE"},
    )


def load_persons(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "ID",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["ID"],
        text_columns={"NAME", "SOURCE", "COMMENTS", "GESLACHT"},
    )


def load_person_function(file_path: Path) -> pd.DataFrame:
    return load_dataframe(
        file_path,
        "PERSON",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["PERSON", "STARTDATE"],
        text_columns={"JOB", "SOURCE", "COMMENTS"},
    )


def load_stock_corporation(csv_file: Path):
    return load_dataframe(csv_file, "STOCK", ["STARTDATE", "ENDDATE"], ["STOCK"], {})


def load_stock_names(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "STOCK",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["STOCK", "STARTDATE"],
        text_columns={"NAME"},
    )

    # Doesn't retain ERROR names
    return df[df.NAME != "ERROR"]


def load_stock_types(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "STOCK",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["STOCK", "STARTDATE"],
        text_columns={"TYPE"},
    )

    # Replace Undefined values by '' in COMPREF column
    df.loc[df["COMPREF"] == "Undefined", "COMPREF"] = ""

    return df


def load_stock_codetype(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "STOCK",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["STOCK", "STARTDATE"],
        text_columns={"CODEYPE_ID", "CODETYPE", "CODE"},
    )

    # Fix column name "CODEYPE_ID" -> "CODETYPE_ID"
    df.columns = ["CODETYPE_ID" if col == "CODEYPE_ID" else col for col in df.columns]

    return df


def scob_references(additional_references=None):
    return database_references(
        BaseItem.SCOB_DATABASE, additional_references=additional_references
    )


def load_locations_types_csv(csv_file: Path):
    """Load SCOB location types info CSV file and return a dict."""

    def str_to_list(value_str):
        return [val.strip() for val in value_str.split(",") if val.strip() != ""]

    location_type_info_dict = {}
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            s_row = {k: v.strip() for k, v in row.items()}
            type_id = s_row["SCOB_LOCATION_TYPE_ID"]
            location_type_info_dict[type_id] = {
                "code": type_id,
                "name": s_row["SCOB_LOCATION_TYPE"],
                "similar_to": s_row["similar to"],
                "labels": {
                    "en": s_row["label_en"],
                    "fr": s_row["label_fr"],
                },
                "description": {
                    "en": s_row["description_en"],
                    "fr": s_row["description_fr"],
                },
                "aliases": {
                    "en": str_to_list(s_row["aliases_en"]),
                    "fr": str_to_list(s_row["aliases_fr"]),
                },
            }
    return location_type_info_dict


def load_legal_status_csv(csv_file: Path):
    """Load SCOB legal status info CSV file and return a dict."""

    def str_to_list(value_str):
        return [val.strip() for val in value_str.split(",") if val.strip() != ""]

    legal_status_info_dict = {}
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            s_row = {k: v.strip() for k, v in row.items()}
            type_id = s_row["SCOB_LEGAL_STATUS_ID"]
            legal_status_info_dict[type_id] = {
                "code": type_id,
                "name": s_row["SCOB_LEGAL_STATUS"],
                "labels": {
                    "en": s_row["label_en"],
                    "fr": s_row["label_fr"],
                },
                "aliases": {
                    "en": str_to_list(s_row["aliases_en"]),
                    "fr": str_to_list(s_row["aliases_fr"]),
                },
            }
    return legal_status_info_dict


def iter_legal_status_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LEGAL_STATUS}.
        ?item wdt:{BaseProperty.SCOB_LEGAL_STATUS_ID} ?legal_status_id
    }}
"""

    def value_func(result):
        return result["legal_status_id"]

    return iter_info_item_id_from_wikibase(query, value_func)


def build_legal_status_item(legal_status_info: Dict[str, Any], wb_handle: WBHandle):
    "Build item from legal status information."

    statements = [
        WDItemID(
            BaseItem.LEGAL_STATUS,
            BaseProperty.INSTANCE_OF,
            references=scob_references(),
        ),
        WDExternalID(
            legal_status_info["code"],
            BaseProperty.SCOB_LEGAL_STATUS_ID,
            references=scob_references(),
            qualifiers=[
                SafeWDString(
                    legal_status_info["name"],
                    BaseProperty.NAMED_AS,
                    is_qualifier=True,
                ),
            ],
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (label, lang) for lang, label in legal_status_info["labels"].items()
        ],
        multilang_aliases=[
            (aliases, lang) for lang, aliases in legal_status_info["aliases"].items()
        ],
    )


def iter_location_type_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LOCATION_TYPE}.
        ?item wdt:{BaseProperty.SCOB_LOCATION_TYPE_ID} ?loc_type_id
    }}
"""

    def value_func(result):
        return result["loc_type_id"]

    return iter_info_item_id_from_wikibase(query, value_func)


def build_location_type_item(location_type_info: Dict[str, Any], wb_handle: WBHandle):
    "Build item from location type information."

    statements = [
        WDItemID(
            BaseItem.LOCATION_TYPE,
            BaseProperty.INSTANCE_OF,
            references=scob_references(),
        ),
        WDExternalID(
            location_type_info["code"],
            BaseProperty.SCOB_LOCATION_TYPE_ID,
            references=scob_references(),
            qualifiers=[
                SafeWDString(
                    location_type_info["name"],
                    BaseProperty.NAMED_AS,
                    is_qualifier=True,
                ),
            ],
        ),
    ]
    if location_type_info["similar_to"] != "":
        statements.append(
            SafeWDString(
                location_type_info["similar_to"],
                BaseProperty.SAID_TO_BE_THE_SAME_AS,
            )
        )

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (label, lang) for lang, label in location_type_info["labels"].items()
        ],
        multilang_descriptions=[
            ("(from scob)" if not description else description + " (from scob)", lang)
            for lang, description in location_type_info["description"].items()
        ],
        multilang_aliases=[
            (aliases, lang)
            for lang, aliases in location_type_info["aliases"].items()
            if aliases
        ],
    )


def create_location_type_items(
    corporation_locations_df: pd.core.frame.DataFrame,
    location_type_info_dict: Dict[str, Any],
    context: Context,
) -> Dict[str, str]:

    wb_handle = context.wb_handle

    # Get existing location type items as a dict (location type id) -> location type item id
    item_id_by_location_type_id = {
        location_type_id: location_type_item_id
        for (
            location_type_id,
            location_type_item_id,
        ) in iter_location_type_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing location type items and complete dict
    for type_id, type_info in location_type_info_dict.items():
        if type_id not in item_id_by_location_type_id:
            location_type_item = build_location_type_item(type_info, wb_handle)

            item_id = wb_handle.write_item(
                location_type_item,
                type_info["name"],
                "location type",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_location_type_id[type_id] = item_id

    return item_id_by_location_type_id


def iter_stock_type_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.STOCK_TYPE}.
        ?item wdt:{BaseProperty.SCOB_STOCK_TYPE_ID} ?stock_type_id
    }}
"""

    def value_func(result):
        return int(result["stock_type_id"])

    return iter_info_item_id_from_wikibase(query, value_func)


def create_corporation_item(
    wb_handle: WBHandle,
    row,
    names_df: pd.DataFrame,
    juridisch_statuut_df: Optional[pd.DataFrame],
    locations_df: Optional[pd.DataFrame],
    item_id_by_city_tuple: Dict[Tuple[str, str], str],
    item_id_by_country_name: Dict[str, str],
    item_id_by_location_type_id: Dict[str, str],
    item_id_by_legal_status_label: Dict[str, str],
) -> Tuple[str, str]:
    """Create or update a Wikibase item corresponding to a SCOB corporation.

    Also create the dependent items like locations.
    """
    statements: List[Any] = [
        WDItemID(BaseItem.CORP, BaseProperty.INSTANCE_OF, references=scob_references())
    ]

    # statements from scob_corporation_sd_ed
    statements.extend(
        [
            WDExternalID(
                str(row.Index),
                BaseProperty.SCOB_CORP_ID,
                references=scob_references(),
            ),
            *date_qualifiers(row, is_qualifier=False, references=scob_references()),
        ]
    )

    # Name statements from scob_corporation_names
    for name_row in names_df.itertuples():
        statements.append(
            SafeWDString(
                name_row.NAME,
                BaseProperty.NAME,
                qualifiers=date_qualifiers(
                    name_row,
                    none_if_empty=True,
                ),
                references=scob_references(),
            )
        )

    # Legal status statements from scob_corporation_juridisch_statuut
    if juridisch_statuut_df is not None:
        for juridisch_statuut_row in juridisch_statuut_df.itertuples():

            legal_status_label = juridisch_statuut_row.JURIDISCH_STATUUT
            legal_status_item = item_id_by_legal_status_label.get(legal_status_label)
            if legal_status_item is None:
                continue

            # Date qualifiers
            qualifiers = date_qualifiers(
                juridisch_statuut_row,
            )

            # Comments qualifier
            comments_value = juridisch_statuut_row.COMMENTS
            if not empty_string(comments_value):
                qualifiers.append(
                    SafeWDString(
                        comments_value, BaseProperty.COMMENTS, is_qualifier=True
                    )
                )

            # Source reference
            source_value = juridisch_statuut_row.SOURCE
            source_references = (
                [SafeWDString(source_value, BaseProperty.STATED_IN, is_reference=True)]
                if not empty_string(source_value)
                else None
            )

            # Legal status
            legal_status_statement = WDItemID(
                legal_status_item,
                BaseProperty.CORP_LEGAL_STATUS,
                qualifiers=qualifiers,
                references=scob_references(additional_references=source_references),
            )
            statements.append(legal_status_statement)

    # Location statements from scob_corporation_locations.csv
    if locations_df is not None:
        for location_row in locations_df.itertuples():

            qualifiers = []

            city_tuple = (
                location_row.CITY,
                location_row.COUNTRY,
            )
            if city_tuple in item_id_by_city_tuple:

                qualifiers.append(
                    WDItemID(
                        item_id_by_city_tuple[city_tuple],
                        BaseProperty.LOCATION_CITY,
                        is_qualifier=True,
                    )
                )
            elif location_row.COUNTRY in item_id_by_country_name:

                qualifiers.append(
                    WDItemID(
                        item_id_by_country_name[location_row.COUNTRY],
                        BaseProperty.LOCATION_COUNTRY,
                        is_qualifier=True,
                    )
                )

            if location_row.ADDRESS:
                qualifiers.append(
                    SafeWDString(
                        location_row.ADDRESS,
                        BaseProperty.LOCATION_ADDRESS,
                        is_qualifier=True,
                    )
                )

            qualifiers.extend(date_qualifiers(location_row))

            location_statement = WDItemID(
                item_id_by_location_type_id.get("1"),  # 1 = headquarters
                BaseProperty.CORP_LOCATION_TYPE,
                qualifiers=qualifiers,
                references=scob_references(),
            )
            statements.append(location_statement)

    corporation_label = find_latest_df_name(names_df)
    return (
        wb_handle.compute_item(
            statements,
            corporation_label,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.CORP},
        ),
        corporation_label,
    )


def create_city_item(
    wb_handle: WBHandle,
    city_name: Optional[str],
    country_item_id: Optional[str],
    item_label: str,
):

    statements = [
        WDItemID(
            BaseItem.CITY,
            BaseProperty.INSTANCE_OF,
            references=scob_references(),
        ),
        SafeWDString(
            city_name,
            BaseProperty.NAME,
            references=scob_references(),
        ),
    ]
    if country_item_id:
        statements.append(
            WDItemID(
                country_item_id,
                BaseProperty.LOCATION_COUNTRY,
                references=scob_references(),
            )
        )

    item = wb_handle.compute_item(statements, item_label)
    return wb_handle.write_item(
        item,
        item_label,
        "city",
        check_if_necessary=False,
    )


def create_position_item(position_label: str, wb_handle: WBHandle):
    """Create position item."""

    statements = [
        WDItemID(
            BaseItem.POSITION,
            BaseProperty.INSTANCE_OF,
            references=scob_references(),
        ),
        SafeWDString(
            position_label,
            BaseProperty.POSITION_TITLE,
            references=scob_references(),
        ),
    ]
    return wb_handle.compute_item(
        statements, position_label, item_label_lang=wb_handle.default_language
    )


def iter_person_function_statement(
    scob_person_id: int,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
):

    for function_row in person_function_df.itertuples():
        scob_corporation_id = function_row.CORPORATION
        corporation_item_id = item_id_by_corporation_id.get(scob_corporation_id)
        if not corporation_item_id:
            log.error(
                "Can't find corporation [%d] for person [%d]",
                scob_corporation_id,
                scob_person_id,
            )
            continue

        qualifiers = []
        position_label = function_row.JOB
        if empty_string(position_label):
            log.error("No position label for person [%d]", scob_person_id)
            continue

        position_item_id = item_id_by_position_label.get(position_label)
        if not position_item_id:
            log.error("Can't find position label '%s' item", position_label)
            continue

        qualifiers.append(
            WDItemID(
                position_item_id,
                BaseProperty.PERSON_FUNCTION,
                is_qualifier=True,
            )
        )
        qualifiers.extend(date_qualifiers(function_row))
        if not empty_string(function_row.COMMENTS):
            qualifiers.append(
                SafeWDString(
                    function_row.COMMENTS,
                    BaseProperty.COMMENTS,
                    is_qualifier=True,
                )
            )

        source_references = (
            [
                SafeWDString(
                    function_row.SOURCE,
                    BaseProperty.STATED_IN,
                    is_reference=True,
                )
            ]
            if not empty_string(function_row.SOURCE)
            else None
        )

        yield WDItemID(
            corporation_item_id,
            BaseProperty.PERSON_FUNCTION_CORPORATION,
            qualifiers=qualifiers,
            references=scob_references(additional_references=source_references),
        )


def create_person_item(
    wb_handle: WBHandle,
    row,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
) -> Tuple[str, str]:

    # statements from persons.csv
    scob_person_id = row.Index

    # Source info from SOURCE column
    person_references = scob_references(
        additional_references=[
            SafeWDString(row.SOURCE, BaseProperty.STATED_IN, is_reference=True)
        ]
        if not empty_string(row.SOURCE)
        else []
    )

    statements = [
        WDItemID(
            BaseItem.PERSON,
            BaseProperty.INSTANCE_OF,
            references=person_references,
        ),
        WDExternalID(
            str(scob_person_id),
            BaseProperty.SCOB_PERSON_ID,
            qualifiers=[
                SafeWDString(row.NAME, BaseProperty.NAMED_AS, is_qualifier=True),
            ],
            references=person_references,
        ),
        WDItemID(
            BaseItem.FEMALE if row.GESLACHT == "V" else BaseItem.MALE,
            BaseProperty.PERSON_GENDER,
            references=person_references,
        ),
    ]

    statements.extend(
        date_qualifiers(
            row,
            is_qualifier=False,
            references=person_references,
            start_date_property_id=BaseProperty.BIRTH_DATE,
            end_date_property_id=BaseProperty.DEATH_DATE,
        )
    )
    if not empty_string(row.COMMENTS):
        statements.append(
            SafeWDString(
                row.COMMENTS,
                BaseProperty.COMMENTS,
                references=person_references,
            )
        )

    # Function statements from person_function
    if person_function_df is not None:

        for function_statement in iter_person_function_statement(
            scob_person_id,
            person_function_df,
            item_id_by_corporation_id,
            item_id_by_position_label,
        ):
            statements.append(function_statement)

    person_label = row.NAME
    return (
        wb_handle.compute_item(
            statements,
            person_label,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.PERSON},
        ),
        person_label,
    )


def iter_city_tuple_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.CITY}.
        ?item p:{BaseProperty.NAME} ?stmt1 {{
            ?item wdt:{BaseProperty.NAME} ?city.
            ?stmt1 prov:wasDerivedFrom ?refnode1.
            ?refnode1 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.SCOB_DATABASE}.
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_COUNTRY} ?stmt3 {{
                ?item wdt:{BaseProperty.LOCATION_COUNTRY} ?country.
                ?stmt3 prov:wasDerivedFrom ?refnode3.
                ?refnode3 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.SCOB_DATABASE}.
            }}
        }}
    }}
    """

    def value_func(result):
        return (
            result.get("city"),
            result.get("country"),
        )

    return iter_info_item_id_from_wikibase(query, value_func)


def create_country_item(
    wb_handle: WBHandle,
    country_name: str,
    references: str,
):
    """Create item with a name property in wikibase."""

    statements = [
        WDItemID(BaseItem.COUNTRY, BaseProperty.INSTANCE_OF, references=references),
        SafeWDString(country_name, BaseProperty.NAME, references=references),
    ]
    item = wb_handle.compute_item(statements, country_name)

    return wb_handle.write_item(
        item,
        country_name,
        "country",
        check_if_necessary=False,
    )


def iter_country_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    """Look for items on their unique string property."""
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.COUNTRY}.
        ?item p:{BaseProperty.NAME} ?stmt {{
            ?item wdt:{BaseProperty.NAME} ?country_name.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.SCOB_DATABASE}.
        }}
    }}
    """
    return iter_info_item_id_from_wikibase(query, lambda res: res.get("country_name"))


def create_city_country_items(
    corporation_locations_df: pd.core.frame.DataFrame,
    context: Context,
):
    def create_country_worker(country_name: str):
        country_id = create_country_item(
            wb_handle,
            country_name,
            scob_references(),
        )
        return country_id

    wb_handle = context.wb_handle

    # COUNTRIES

    # Get existing country items
    item_id_by_country_name = {
        country_name: country_item_id
        for (country_name, country_item_id) in iter_country_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing country items and update dict
    missing_country_list = [
        country_name
        for country_name in corporation_locations_df["COUNTRY"].unique().tolist()
        if country_name not in item_id_by_country_name
    ]

    log.info("== COUNTRIES ==")
    country_item_ids = parallel_item_process(
        create_country_worker, missing_country_list, max_workers=context.max_workers
    )

    item_id_by_country_name.update(zip(missing_country_list, country_item_ids))

    country_name_by_item_id = {v: k for k, v in item_id_by_country_name.items()}

    # CITIES

    # Get existing city items as a dict (city name, country name) -> city item id
    item_id_by_city_tuple = {}
    for city_tuple, city_item_id in iter_city_tuple_item_id_from_wikibase(
        wb_handle.iter_info_item_id_from_wikibase
    ):
        city_name, country_item_url = city_tuple
        name_city_tuple = (
            city_name,
            country_name_by_item_id[itemid_from_url(country_item_url)],
        )
        item_id_by_city_tuple[name_city_tuple] = city_item_id

    def create_city_worker(name_city_tuple: Tuple[str, str]):
        city_name, country_name = name_city_tuple
        return create_city_item(
            wb_handle,
            city_name,
            item_id_by_country_name.get(country_name),
            city_name,
        )

    # Create missing city set
    missing_city_set = set()
    for row in corporation_locations_df.itertuples():
        city_name_tuple = (row.CITY, row.COUNTRY)
        if row.CITY is not None and city_name_tuple not in item_id_by_city_tuple:
            missing_city_set.add(city_name_tuple)
    missing_city_country_tuple_list = sorted(missing_city_set)

    # Create missing cities
    log.info("== CITIES ==")
    city_tuple_item_ids = parallel_item_process(
        create_city_worker,
        missing_city_country_tuple_list,
        max_workers=context.max_workers,
    )

    item_id_by_city_tuple.update(
        zip(missing_city_country_tuple_list, city_tuple_item_ids)
    )

    return item_id_by_city_tuple, item_id_by_country_name


def create_legal_status_items(
    legal_status_info_dict: Dict[str, Any],
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing legal status items as a dict legal status id -> legal status item id
    item_id_by_legal_status_id = {
        legal_status_id: legal_status_item_id
        for (
            legal_status_id,
            legal_status_item_id,
        ) in iter_legal_status_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Iterate on legal status info dict
    item_id_by_legal_status_label = {}
    for legal_status_id, legal_status_info in legal_status_info_dict.items():
        legal_status_label = legal_status_info["name"]
        if legal_status_id not in item_id_by_legal_status_id:
            log.debug(legal_status_label)
            item = build_legal_status_item(legal_status_info, wb_handle)
            item_id = wb_handle.write_item(
                item,
                legal_status_label,
                "legal status",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_legal_status_label[legal_status_label] = item_id

    return item_id_by_legal_status_label


def compute_item_id_by_corp_id(wb_handle):
    """Compute corporation id to item id dict."""
    item_id_by_corp_id = wb_handle.id_mapper(BaseProperty.SCOB_CORP_ID)
    if item_id_by_corp_id is None:
        return {}
    return {int(k): v for k, v in item_id_by_corp_id.items()}


def create_corporation_items(
    corporation_sd_ed_df: pd.core.frame.DataFrame,
    corporation_names_df: pd.core.frame.DataFrame,
    corporation_juridisch_statuut_df: pd.core.frame.DataFrame,
    corporation_locations_df: pd.core.frame.DataFrame,
    item_id_by_city_tuple: Dict[Tuple[str, str], str],
    item_id_by_country_name: Dict[str, str],
    item_id_by_location_type_id: Dict[str, str],
    item_id_by_legal_status_label: Dict[str, str],
    context: Context,
):
    wb_handle = context.wb_handle

    # Compute corporation id to item id dict
    item_id_by_corp_id = compute_item_id_by_corp_id(wb_handle)

    # build corporation list to create
    missing_corporation_row_list = []
    for row in corporation_sd_ed_df.itertuples():
        scob_corporation_id = row.Index

        if (
            scob_corporation_id not in item_id_by_corp_id
            and scob_corporation_id in corporation_names_df.index
        ):
            missing_corporation_row_list.append(row)

    def create_corporation_worker(row):
        scob_corporation_id = row.Index
        names_df = corporation_names_df.loc[[scob_corporation_id]]

        locations_df = (
            corporation_locations_df.loc[[scob_corporation_id]]
            if scob_corporation_id in corporation_locations_df.index
            else None
        )
        if locations_df is None:
            log.debug(
                "No location information for corporation [%d]", scob_corporation_id
            )

        juridisch_statuut_df = (
            corporation_juridisch_statuut_df.loc[[scob_corporation_id]]
            if scob_corporation_id in corporation_juridisch_statuut_df.index
            else None
        )
        if juridisch_statuut_df is None:
            log.debug(
                "No legal status information for corporation [%d]", scob_corporation_id
            )

        item, item_label = create_corporation_item(
            wb_handle,
            row,
            names_df,
            juridisch_statuut_df,
            locations_df,
            item_id_by_city_tuple,
            item_id_by_country_name,
            item_id_by_location_type_id,
            item_id_by_legal_status_label,
        )
        return wb_handle.write_item(
            item, scob_corporation_id, "corporation", label=item_label
        )

    log.info("== CORPORATIONS ==")
    corporation_item_ids = parallel_item_process(
        create_corporation_worker,
        missing_corporation_row_list,
        max_workers=context.max_workers,
    )

    item_id_by_corp_id.update(
        zip([row.Index for row in missing_corporation_row_list], corporation_item_ids)
    )

    return item_id_by_corp_id


def iter_person_scob_id_item_id_from_wikibase(
    iter_info_item_id_from_wikibase: Callable,
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.PERSON}.
        ?item wdt:{BaseProperty.SCOB_PERSON_ID} ?scob_person_id
    }}
    """

    def value_func(result):
        return result["scob_person_id"]

    return iter_info_item_id_from_wikibase(query, value_func)


def iter_position_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.POSITION}.
        ?item p:{BaseProperty.POSITION_TITLE} ?stmt {{
            ?item wdt:{BaseProperty.POSITION_TITLE} ?position_title.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.SCOB_DATABASE}.
        }}
    }}
    """

    def value_func(result):
        return result["position_title"]

    return iter_info_item_id_from_wikibase(query, value_func)


def compute_item_id_by_position_label(wb_handle: WBHandle):
    """Get existing position items as a dict position item label -> position item id."""
    return {
        position_title: position_item_id
        for (position_title, position_item_id) in iter_position_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }


def create_position_items(
    person_function_df: pd.core.frame.DataFrame,
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing position items as a dict position item label -> position item id
    item_id_by_position_label = compute_item_id_by_position_label(wb_handle)

    def create_position_worker(position_label: str):
        item = create_position_item(position_label, wb_handle)
        return wb_handle.write_item(item, position_label, "position")

    # Create missing position list
    missing_position_label_list = sorted(
        [
            position_label
            for position_label in person_function_df["JOB"].unique().tolist()
            if position_label not in item_id_by_position_label
        ]
    )

    log.info("== POSITIONS ==")
    position_item_ids = parallel_item_process(
        create_position_worker,
        missing_position_label_list,
        max_workers=context.max_workers,
    )

    item_id_by_position_label.update(
        zip(missing_position_label_list, position_item_ids)
    )

    return item_id_by_position_label


def create_person_items(
    persons_df: pd.core.frame.DataFrame,
    person_function_df: pd.core.frame.DataFrame,
    item_id_by_corp_id: Dict[int, str],
    item_id_by_position_label: Dict[str, str],
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing person items as a dict scob person id -> item id
    item_id_by_person_id = {
        int(scob_person_id): person_item_id
        for (
            scob_person_id,
            person_item_id,
        ) in iter_person_scob_id_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing person list
    missing_person_row_list = [
        row
        for row in persons_df.itertuples()
        if row.Index not in item_id_by_person_id and row.NAME is not None
    ]

    # Create person worker
    def create_person_worker(row):
        scob_person_id = row.Index
        function_df = (
            person_function_df.loc[[scob_person_id]]
            if scob_person_id in person_function_df.index
            else None
        )

        item, item_label = create_person_item(
            wb_handle,
            row,
            function_df,
            item_id_by_corp_id,
            item_id_by_position_label,
        )
        wb_handle.write_item(item, scob_person_id, "person", label=item_label)

    log.info("== PERSONS ==")
    parallel_item_process(
        create_person_worker, missing_person_row_list, max_workers=context.max_workers
    )


def build_stock_type_item(
    stock_type_id: str,
    stock_type_label: str,
    wb_handle: WBHandle,
):
    """Build stock type item."""

    statements = [
        WDItemID(
            BaseItem.STOCK_TYPE,
            BaseProperty.INSTANCE_OF,
            references=scob_references(),
        ),
        WDExternalID(
            str(stock_type_id),
            BaseProperty.SCOB_STOCK_TYPE_ID,
            references=scob_references(),
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[(stock_type_label, lang) for lang in wb_handle.language_list],
    )


def create_stock_type_items(
    stock_type_df: pd.DataFrame, context: Context
) -> Dict[int, str]:

    wb_handle = context.wb_handle

    # Get existing stock type items as a dict (stock type id) -> stock type item id
    item_id_by_stock_type_id = {
        stock_type_id: stock_type_item_id
        for (
            stock_type_id,
            stock_type_item_id,
        ) in iter_stock_type_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing stock type list
    missing_stock_type_list = [
        (row.TYPE_ID, row.TYPE)
        for row in stock_type_df[["TYPE_ID", "TYPE"]].drop_duplicates().itertuples()
        if row.TYPE_ID not in item_id_by_stock_type_id
    ]

    def create_stock_type_worker(stock_type_tuple):
        stock_type_id, stock_type_label = stock_type_tuple
        stock_type_item = build_stock_type_item(
            stock_type_id, stock_type_label, wb_handle
        )
        return wb_handle.write_item(
            stock_type_item,
            stock_type_label,
            "stock type",
            check_if_necessary=False,
        )

    log.info("== STOCK TYPES ==")
    stock_type_item_ids = parallel_item_process(
        create_stock_type_worker,
        missing_stock_type_list,
        max_workers=context.max_workers,
    )

    item_id_by_stock_type_id.update(zip(missing_stock_type_list, stock_type_item_ids))

    return item_id_by_stock_type_id


def iter_stock_codetype_item_id_from_wikibase(
    iter_info_item_id_from_wikibase: Callable,
):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.STOCK_CODETYPE}.
        ?item wdt:{BaseProperty.SCOB_STOCK_CODETYPE_ID} ?stock_codetype_id
    }}
"""

    def value_func(result):
        return result["stock_codetype_id"]

    return iter_info_item_id_from_wikibase(query, value_func)


def build_stock_codetype_item(
    stock_codetype_id: str,
    stock_codetype_label: str,
    wb_handle: WBHandle,
):
    "Build item from stock code type information."

    statements = [
        WDItemID(
            BaseItem.STOCK_CODETYPE,
            BaseProperty.INSTANCE_OF,
            references=scob_references(),
        ),
        WDExternalID(
            stock_codetype_id,
            BaseProperty.SCOB_STOCK_CODETYPE_ID,
            references=scob_references(),
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (stock_codetype_label, lang) for lang in wb_handle.language_list
        ],
    )


def create_stock_codetype_items(
    stock_codetype_df: pd.DataFrame,
    context: Context,
) -> Dict[int, str]:

    wb_handle = context.wb_handle

    # Get existing stock code type items as a dict (stock code type id) -> stock code type item id
    item_id_by_stock_codetype_id = {
        stock_codetype_id: stock_codetype_item_id
        for (
            stock_codetype_id,
            stock_codetype_item_id,
        ) in iter_stock_codetype_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing stock codetype items and complete dict
    # Note: Too few codetype to create them using parallelism
    for row in (
        stock_codetype_df[["CODETYPE_ID", "CODETYPE"]].drop_duplicates().itertuples()
    ):
        stock_codetype_id = row.CODETYPE_ID
        stock_codetype_label = row.CODETYPE
        if stock_codetype_id not in item_id_by_stock_codetype_id:
            stock_codetype_item = build_stock_codetype_item(
                stock_codetype_id, stock_codetype_label, wb_handle
            )

            item_id = wb_handle.write_item(
                stock_codetype_item,
                stock_codetype_label,
                "stock code type",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_stock_codetype_id[stock_codetype_id] = item_id

    return item_id_by_stock_codetype_id


def iter_stock_scob_id_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.STOCK}.
        ?item wdt:{BaseProperty.SCOB_STOCK_ID} ?scob_stock_id
    }}
    """

    def value_func(result):
        return result["scob_stock_id"]

    return iter_info_item_id_from_wikibase(query, value_func)


def create_stock_item(
    wb_handle: WBHandle,
    stock_id,
    corporations_df: pd.DataFrame,
    names_df: pd.DataFrame,
    types_df: pd.DataFrame,
    codetype_df: pd.DataFrame,
    item_id_by_stock_type_id: Dict[int, str],
    item_id_by_stock_codetype_id: Dict[int, str],
    item_id_by_corporation_id: Dict[int, str],
) -> Tuple[str, str]:

    stock_references = scob_references()

    statements = [
        WDItemID(
            BaseItem.STOCK,
            BaseProperty.INSTANCE_OF,
            references=stock_references,
        ),
        WDExternalID(
            str(stock_id),
            BaseProperty.SCOB_STOCK_ID,
            references=stock_references,
        ),
    ]

    # Name statements from stock_names
    if names_df is not None:

        for name_row in names_df.itertuples():

            statements.append(
                SafeWDString(
                    name_row.NAME,
                    BaseProperty.NAME,
                    qualifiers=date_qualifiers(name_row, none_if_empty=True),
                    references=stock_references,
                )
            )

    # Corporation statements from stock_corporations
    if corporations_df is not None:

        for corp_row in corporations_df.itertuples():

            corp_item_id = item_id_by_corporation_id.get(corp_row.CORPORATION)
            if corp_item_id is None:
                log.warning("Unknown corporation [%d]", corp_row.CORPORATION)
                continue

            statements.append(
                WDItemID(
                    corp_item_id,
                    BaseProperty.STOCK_CORPORATION,
                    qualifiers=date_qualifiers(corp_row, none_if_empty=True),
                    references=stock_references,
                )
            )

    # type statements from stock_types
    if types_df is not None:

        for type_row in types_df.itertuples():

            stock_type_qualifiers = []

            # TODO: réponse David
            # # compref type
            # stock_compref = type_row.COMPREF

            # if stock_compref:
            #     stock_type_qualifiers.append(
            #         SafeWDString(
            #             stock_compref,
            #             BaseProperty.EXACT_STOCK_TYPE,
            #             is_qualifier=True,
            #         )
            #     )
            stock_type_qualifiers.extend(date_qualifiers(type_row))

            # type
            stock_type_id = type_row.TYPE_ID

            if stock_type_id:
                statements.append(
                    WDItemID(
                        item_id_by_stock_type_id.get(stock_type_id),
                        BaseProperty.STOCK_TYPE,
                        qualifiers=stock_type_qualifiers,
                        references=stock_references,
                    )
                )

    if codetype_df is not None:

        for codetype_row in codetype_df.itertuples():

            codetype_qualifiers = [
                WDExternalID(
                    codetype_row.CODE, BaseProperty.STOCK_CODE, is_qualifier=True
                ),
                *date_qualifiers(codetype_row),
            ]

            codetype_item_id = item_id_by_stock_codetype_id.get(
                codetype_row.CODETYPE_ID
            )
            if codetype_item_id is None:
                log.error("Unknown codetype [%s]", codetype_row.CODETYPE)
                continue

            statements.append(
                WDItemID(
                    codetype_item_id,
                    BaseProperty.STOCK_CODETYPE,
                    qualifiers=codetype_qualifiers,
                    references=stock_references,
                )
            )

    stock_label = find_latest_df_name(names_df)
    return (
        wb_handle.compute_item(
            statements,
            stock_label,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.STOCK},
        ),
        stock_label,
    )


def create_stock_items(
    stock_corporation_df: pd.core.frame.DataFrame,
    stock_names_df: pd.core.frame.DataFrame,
    stock_types_df: pd.core.frame.DataFrame,
    stock_codetype_df: pd.core.frame.DataFrame,
    item_id_by_stock_type_id: Dict[int, str],
    item_id_by_stock_codetype_id: Dict[int, str],
    item_id_by_corporation_id: Dict[int, str],
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing stock items as a dict scob stock id -> item id
    item_id_by_stock_id = {
        int(scob_stock_id): stock_item_id
        for (scob_stock_id, stock_item_id,) in iter_stock_scob_id_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing stock list
    missing_stock_list = [
        (stock_id, names_df)
        for (stock_id, names_df) in stock_names_df.groupby(["STOCK"])
        if stock_id not in item_id_by_stock_id
    ]

    def create_stock_worker(stock_tuple):
        stock_id, names_df = stock_tuple

        # Associated corporations
        corporations_df = (
            stock_corporation_df.loc[[stock_id]]
            if stock_id in stock_corporation_df.index
            else None
        )

        # Associated types
        types_df = (
            stock_types_df.loc[[stock_id]] if stock_id in stock_types_df.index else None
        )

        # Associated codetype
        codetype_df = (
            stock_codetype_df.loc[[stock_id]]
            if stock_id in stock_codetype_df.index
            else None
        )

        item, item_label = create_stock_item(
            wb_handle,
            stock_id,
            corporations_df,
            names_df,
            types_df,
            codetype_df,
            item_id_by_stock_type_id,
            item_id_by_stock_codetype_id,
            item_id_by_corporation_id,
        )

        wb_handle.write_item(
            item,
            stock_id,
            "stock",
            label=item_label,
        )

    log.info("== STOCKS ==")
    parallel_item_process(
        create_stock_worker, missing_stock_list, max_workers=context.max_workers
    )


def import_csv_data(
    csv_files_dir: Path,
    context: Context,
):
    """Import scob entities from CSV data."""

    # Load CSV sources
    log.info("Loading dataframes...")
    corporation_sd_ed_df = load_corporations(
        csv_files_dir / "scob_corporation_sd_ed.csv"
    )
    corporation_names_df = load_corporation_names(
        csv_files_dir / "scob_corporation_names.csv"
    )
    corporation_locations_df = load_corporation_locations(
        csv_files_dir / "scob_corporation_locations.csv"
    )
    corporation_juridisch_statuut_df = load_corporation_legal_status(
        csv_files_dir / "scob_corporation_juridisch_statuut.csv"
    )
    persons_df = load_persons(csv_files_dir / "scob_persons.csv")
    person_function_df = load_person_function(
        csv_files_dir / "scob_person_function.csv"
    )
    stock_corporation_df = load_stock_corporation(
        csv_files_dir / "scob_stock_corporation.csv"
    )
    stock_names_df = load_stock_names(csv_files_dir / "scob_stock_names.csv")
    stock_types_df = load_stock_types(csv_files_dir / "scob_stock_type.csv")
    stock_codetype_df = load_stock_codetype(csv_files_dir / "scob_stock_codetype.csv")
    log.info("done.")

    # Load CSV files from "resources" dir, containing hand-crafted data,
    # not exported from DB.
    location_type_info_dict = load_locations_types_csv(
        PROJECT_DIR / "resources" / "scob_location_types.csv"
    )
    legal_status_info_dict = load_legal_status_csv(
        PROJECT_DIR / "resources" / "scob_legal_status.csv"
    )

    # Create items in wikibase

    # Create cities items (city, country)
    item_id_by_city_tuple, item_id_by_country_name = create_city_country_items(
        corporation_locations_df,
        context,
    )

    # Create location type items
    item_id_by_location_type_id = create_location_type_items(
        corporation_locations_df,
        location_type_info_dict,
        context,
    )

    # Create legal status items
    item_id_by_legal_status_label = create_legal_status_items(
        legal_status_info_dict,
        context,
    )

    # Create corporation items
    item_id_by_corp_id = create_corporation_items(
        corporation_sd_ed_df,
        corporation_names_df,
        corporation_juridisch_statuut_df,
        corporation_locations_df,
        item_id_by_city_tuple,
        item_id_by_country_name,
        item_id_by_location_type_id,
        item_id_by_legal_status_label,
        context,
    )

    # Create person position items
    item_id_by_position_label = create_position_items(
        person_function_df,
        context,
    )

    # Create person items
    create_person_items(
        persons_df,
        person_function_df,
        item_id_by_corp_id,
        item_id_by_position_label,
        context,
    )

    # Create stock type items
    item_id_by_stock_type_id = create_stock_type_items(stock_types_df, context)

    # Create stock code type items
    item_id_by_stock_codetype_id = create_stock_codetype_items(
        stock_codetype_df,
        context,
    )

    # create stock items
    create_stock_items(
        stock_corporation_df,
        stock_names_df,
        stock_types_df,
        stock_codetype_df,
        item_id_by_stock_type_id,
        item_id_by_stock_codetype_id,
        item_id_by_corp_id,
        context,
    )


def update_person_functions(
    id_name_person_list: List[Tuple[int, str]], csv_files_dir: Path, context: Context
):
    # Load person functions
    person_function_df = load_person_function(
        csv_files_dir / "scob_person_function.csv"
    )

    wb_handle = context.wb_handle

    # Get existing person items as a dict dfih person id -> item id
    item_id_by_person_id = {
        int(scob_person_id): person_item_id
        for (
            scob_person_id,
            person_item_id,
        ) in iter_person_scob_id_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }
    item_id_by_corporation_id = compute_item_id_by_corp_id(wb_handle)
    item_id_by_position_label = compute_item_id_by_position_label(wb_handle)

    person_tuples_to_handle = [
        (person_id, person_name)
        for (person_id, person_name) in id_name_person_list
        if person_id in item_id_by_person_id and person_id in person_function_df.index
    ]

    def update_person_worker(person_tuple: Tuple[int, str]):
        person_id, person_name = person_tuple
        person_item_id = item_id_by_person_id.get(person_id)
        function_df = person_function_df.loc[[person_id]]
        log.debug("Updating %r (%s)", person_name, person_item_id)
        statements = [
            stmt
            for stmt in iter_person_function_statement(
                person_id,
                function_df,
                item_id_by_corporation_id,
                item_id_by_position_label,
            )
        ]
        item = wb_handle.item_engine(wd_item_id=person_item_id, data=statements)
        item.write(wb_handle.login_instance)

    parallel_item_process(
        update_person_worker, person_tuples_to_handle, max_workers=context.max_workers
    )
