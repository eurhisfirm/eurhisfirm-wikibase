import csv
import logging
from operator import itemgetter
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Tuple

import numpy as np
import pandas as pd
import typer
from wikidataintegrator.wdi_core import WDExternalID, WDItemID, WDQuantity, WDTime

from base_entities_gen import BaseItem, BaseProperty

from .common_import import database_references, date_qualifiers
from .dataframe_utils import empty_string, int_to_str_series, load_dataframe
from .wdi_utils import (
    Context,
    SafeWDString,
    WBHandle,
    find_latest_df_name,
    format_wikibase_date,
    parallel_item_process,
)
from .wikibase_utils import itemid_from_url

DFIH_LABEL_LANGUAGE = "fr"

PROJECT_DIR = Path(__file__).parent.parent

log = logging.getLogger(__name__)


def dfih_references(additional_references: List[Any] = None):
    return database_references(
        BaseItem.DFIH_DATABASE, additional_references=additional_references
    )


def load_corporations(csv_file: Path):
    return load_dataframe(
        csv_file,
        "ID",
        date_columns=["STARTDATE", "ENDDATE"],
        text_columns=["PUBLIC_STATUS"],
    )


def load_corporation_names(csv_file: Path):
    return load_dataframe(
        csv_file,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"NAME", "SOURCE"},
    )


def load_corporation_locations(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "CORPORATION",
        sort_columns=["CORPORATION", "IN_YEARBOOK_FROM"],
        text_columns={
            "LOCATION_TYPE",
            "ADDRESS",
            "CITY",
            "DISTRICT",
            "COUNTRY",
        },
    )

    # Convert CITY_ID, DISTRICT_ID, COUNTRY_ID from Int to string
    # as wikibase external ids are string
    #
    # This is not done using as_type(str) that convert int to str float representation
    # (e.g. "19.0") and create "nan" values
    #
    for col_name in ("CITY_ID", "DISTRICT_ID", "COUNTRY_ID"):
        df[col_name] = df[col_name].apply(int_to_str_series)

    return df


def load_corporation_juridical_status(csv_file: Path):
    return load_dataframe(
        csv_file,
        "CORPORATION",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["CORPORATION", "STARTDATE"],
        text_columns={"JURIDICAL_STATUS", "COMMENTS", "SOURCE"},
    )


def load_persons(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "ID",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["ID", "STARTDATE"],
        text_columns={"NAME", "SOURCE", "COMMENTS"},
    )

    # Don't keep ids history
    df = df[df["NEW_ID"].isnull()]

    # Drop unwanted "NEW_ID" column
    df = df.drop("NEW_ID", axis=1)

    return df


def load_person_names(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "ID",
        sort_columns=["ID"],
        text_columns={"NAME_DFIH", "PRENOM(s)", "TITRE", "NOM"},
    )

    # Don't keep company names
    df = df[df["FLAG_ENTREPRISE"].isnull()]

    # Don't keep items to be checked
    df = df[df["A_VERIFIER"].isnull()]

    # Drop unwanted columns
    df = df.drop("FLAG_ENTREPRISE", axis=1)
    df = df.drop("A_VERIFIER", axis=1)

    return df


def load_person_function(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "PERSON",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["PERSON", "STARTDATE"],
        text_columns={"JOB", "SOURCE", "COMMENTS"},
    )

    # Drop unused OLD_PERSON_ID column
    df = df.drop("OLD_PERSON_ID", axis=1)

    # Convert JOB_ID column type from int to str
    df["JOB_ID"] = df["JOB_ID"].apply(int_to_str_series)

    return df


def load_stock_corporation(csv_file: Path):
    return load_dataframe(csv_file, "STOCK", ["STARTDATE", "ENDDATE"], ["STOCK"], {})


def load_stock_names(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "STOCK",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["STOCK", "STARTDATE"],
        text_columns={"NAME"},
    )

    # Drop unused STOCKEXCHANGE column
    df = df.drop("STOCKEXCHANGE", axis=1)

    return df


def load_stock_types(csv_file: Path):
    df = load_dataframe(
        csv_file,
        "STOCK",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["STOCK", "STARTDATE"],
        text_columns={"TYPE"},
    )

    # Replace UNKNOWN values by '' in TYPE column
    df.loc[df["TYPE"] == "UNKNOWN", "TYPE"] = ""

    return df


def load_stock_codetype(csv_file: Path):
    return load_dataframe(
        csv_file,
        "STOCK",
        date_columns=["STARTDATE", "ENDDATE"],
        sort_columns=["STOCK", "STARTDATE"],
        text_columns={"CODETYPE", "CODE"},
    )


def load_stock_spot_prices(csv_file: Path):
    return load_dataframe(
        csv_file,
        "ID",
        date_columns=["DAY"],
        sort_columns=["ID"],
        text_columns={"SECTOR_NAME"},
    )


def iter_country_item_id_from_wikibase(iter_info_item_id_from_wikibase: Callable):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.COUNTRY}.
        ?item p:{BaseProperty.DFIH_COUNTRY_ID} ?stmt1 {{
            ?item wdt:{BaseProperty.DFIH_COUNTRY_ID} ?country_id.
            ?stmt1 prov:wasDerivedFrom ?refnode1.
            ?refnode1 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
        }}
    }}
    """

    def value_func(result):
        return result["country_id"]

    return iter_info_item_id_from_wikibase(query, value_func)


def iter_district_tuple_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        {{
            ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.DISTRICT}.
            ?item p:{BaseProperty.DFIH_DISTRICT_ID} ?stmt1 {{
                ?item wdt:{BaseProperty.DFIH_DISTRICT_ID} ?district_id.
                ?stmt1 prov:wasDerivedFrom ?refnode1.
                ?refnode1 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_COUNTRY} ?stmt2 {{
                ?item wdt:{BaseProperty.LOCATION_COUNTRY} ?country.
                ?stmt2 prov:wasDerivedFrom ?refnode2.
                ?refnode2 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
            }}
        }}
    }}
    """

    def value_func(result):
        return (
            result.get("district_id"),
            result.get("country"),
        )

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def iter_city_tuple_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        {{
            ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.CITY}.
            ?item p:{BaseProperty.DFIH_CITY_ID} ?stmt1 {{
                ?item wdt:{BaseProperty.DFIH_CITY_ID} ?city_id.
                ?stmt1 prov:wasDerivedFrom ?refnode1.
                ?refnode1 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
            }}
        }}

        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_DISTRICT} ?stmt2 {{
                ?item wdt:{BaseProperty.LOCATION_DISTRICT} ?district.
                ?stmt2 prov:wasDerivedFrom ?refnode2.
                ?refnode2 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
            }}
        }}
        OPTIONAL {{
            ?item p:{BaseProperty.LOCATION_COUNTRY} ?stmt3 {{
                ?item wdt:{BaseProperty.LOCATION_COUNTRY} ?country.
                ?stmt3 prov:wasDerivedFrom ?refnode3.
                ?refnode3 pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
            }}
        }}
    }}
    """

    def value_func(result):
        return (
            result.get("city_id"),
            result.get("district"),
            result.get("country"),
        )

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def create_country_item(
    country_id: str,
    country_name: str,
    wb_handle: WBHandle,
) -> str:

    statements = [
        WDItemID(
            BaseItem.COUNTRY,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            country_id,
            BaseProperty.DFIH_COUNTRY_ID,
            references=dfih_references(),
            qualifiers=[
                SafeWDString(country_name, BaseProperty.NAMED_AS, is_qualifier=True)
            ],
        ),
    ]

    item = wb_handle.compute_item(statements, country_name)

    return wb_handle.write_item(
        item,
        country_name,
        "country",
        check_if_necessary=False,
    )


def create_district_item(
    wb_handle: WBHandle,
    district_name: str,
    district_id: str,
    country_item_id: Optional[str],
    item_label: str,
) -> str:

    statements = [
        WDItemID(
            BaseItem.DISTRICT,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            district_id,
            BaseProperty.DFIH_DISTRICT_ID,
            references=dfih_references(),
            qualifiers=[
                SafeWDString(district_name, BaseProperty.NAMED_AS, is_qualifier=True)
            ],
        ),
    ]

    if country_item_id:
        statements.append(
            WDItemID(
                country_item_id,
                BaseProperty.LOCATION_COUNTRY,
                references=dfih_references(),
            )
        )

    item = wb_handle.compute_item(statements, item_label)
    return wb_handle.write_item(
        item,
        item_label,
        "district",
        check_if_necessary=False,
    )


def create_city_item(
    wb_handle: WBHandle,
    city_name: str,
    city_id: str,
    district_item_id: Optional[str],
    country_item_id: Optional[str],
    item_label: str,
) -> str:

    statements = [
        WDItemID(
            BaseItem.CITY,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            city_id,
            BaseProperty.DFIH_CITY_ID,
            references=dfih_references(),
            qualifiers=[
                SafeWDString(city_name, BaseProperty.NAMED_AS, is_qualifier=True)
            ],
        ),
    ]
    if district_item_id:
        statements.append(
            WDItemID(
                district_item_id,
                BaseProperty.LOCATION_DISTRICT,
                references=dfih_references(),
            )
        )

    if country_item_id:
        statements.append(
            WDItemID(
                country_item_id,
                BaseProperty.LOCATION_COUNTRY,
                references=dfih_references(),
            )
        )

    item = wb_handle.compute_item(statements, item_label)
    return wb_handle.write_item(
        item,
        item_label,
        "city",
        check_if_necessary=False,
    )


def create_country_district_city_items(
    corporation_locations_df: pd.core.frame.DataFrame,
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing country items
    item_id_by_country_id = {
        country_id: country_item_id
        for (country_id, country_item_id) in iter_country_item_id_from_wikibase(
            wb_handle.iter_info_item_id_from_wikibase
        )
    }

    # Create missing country items
    country_name_by_country_id = dict(
        list(
            zip(
                corporation_locations_df["COUNTRY_ID"].tolist(),
                corporation_locations_df["COUNTRY"].tolist(),
            )
        )
    )
    missing_country_list = [
        country_id
        for country_id in corporation_locations_df["COUNTRY_ID"].unique().tolist()
        if country_id is not None and country_id not in item_id_by_country_id
    ]

    def create_country_worker(country_id):
        return create_country_item(
            country_id,
            country_name_by_country_id.get(country_id, f"country[{country_id}"),
            wb_handle,
        )

    log.info("== COUNTRIES ==")
    country_item_ids = parallel_item_process(
        create_country_worker, missing_country_list, max_workers=context.max_workers
    )
    item_id_by_country_id.update(zip(missing_country_list, country_item_ids))

    # Build reverse dict to be able to retrieve a country id by its item id
    country_id_by_item_id = {v: k for k, v in item_id_by_country_id.items()}

    # Get existing district items as a dict (district id, country id) -> district item id
    item_id_by_district_tuple = {}
    for district_tuple, district_item_id in iter_district_tuple_item_id_from_wikibase(
        wb_handle,
    ):
        district_id, country_item_id = district_tuple
        name_district_tuple = (
            district_id,
            country_id_by_item_id.get(itemid_from_url(country_item_id)),
        )
        item_id_by_district_tuple[name_district_tuple] = district_item_id

    # Map district id to district name
    district_name_by_district_id = dict(
        (row.DISTRICT_ID, row.DISTRICT)
        for row in corporation_locations_df[["DISTRICT_ID", "DISTRICT"]]
        .drop_duplicates()
        .itertuples()
    )

    missing_district_set = {
        (row.DISTRICT_ID, row.COUNTRY_ID)
        for row in corporation_locations_df[["DISTRICT_ID", "DISTRICT", "COUNTRY_ID"]]
        .drop_duplicates()
        .itertuples()
        if row.DISTRICT is not None
        and row.DISTRICT_ID is not None
        and (row.DISTRICT_ID, row.COUNTRY_ID) not in item_id_by_district_tuple
    }
    missing_district_tuple_list = list(missing_district_set)

    def create_district_worker(district_tuple):
        district_id, country_id = district_tuple
        district_name = district_name_by_district_id[district_id]
        return create_district_item(
            wb_handle,
            district_name,
            district_id,
            item_id_by_country_id.get(country_id),
            district_name,
        )

    log.info("== DISTRICTS ==")
    district_item_ids = parallel_item_process(
        create_district_worker,
        missing_district_tuple_list,
        max_workers=context.max_workers,
    )
    item_id_by_district_tuple.update(
        zip(missing_district_tuple_list, district_item_ids)
    )

    # Reverse dict
    district_id_by_item_id = {v: k[0] for k, v in item_id_by_district_tuple.items()}

    # Get existing city items as a dict (city id, district id, country id) -> city item id
    item_id_by_city_tuple = {}
    for city_tuple, city_item_id in iter_city_tuple_item_id_from_wikibase(wb_handle):
        city_id, district_item_url, country_item_url = city_tuple
        id_city_tuple = (
            city_id,
            district_id_by_item_id[itemid_from_url(district_item_url)]
            if district_item_url
            else None,
            country_id_by_item_id[itemid_from_url(country_item_url)]
            if country_item_url
            else None,
        )
        item_id_by_city_tuple[id_city_tuple] = city_item_id

    # Create missing city items
    # and update dict
    city_name_by_city_id = dict(
        (row.CITY_ID, row.CITY)
        for row in corporation_locations_df[["CITY_ID", "CITY"]]
        .drop_duplicates()
        .itertuples()
        if row.CITY_ID is not None
    )

    # Missing items
    missing_city_set = {
        (row.CITY_ID, row.DISTRICT_ID, row.COUNTRY_ID)
        for row in corporation_locations_df[["CITY_ID", "DISTRICT_ID", "COUNTRY_ID"]]
        .drop_duplicates()
        .itertuples()
        if row.CITY_ID is not None
        and city_name_by_city_id[row.CITY_ID] is not None
        and (row.CITY_ID, row.DISTRICT_ID, row.COUNTRY_ID) not in item_id_by_city_tuple
    }
    missing_city_tuple_list = sorted(missing_city_set, key=itemgetter(0))

    # Worker
    def create_city_worker(city_tuple):
        city_id, district_id, country_id = city_tuple
        city_name = city_name_by_city_id[city_id]
        return create_city_item(
            wb_handle,
            city_name,
            city_id,
            item_id_by_district_tuple.get((district_id, country_id)),
            item_id_by_country_id.get(country_id),
            city_name,
        )

    log.info("== CITIES ==")
    city_item_ids = parallel_item_process(
        create_city_worker, missing_city_tuple_list, max_workers=context.max_workers
    )
    item_id_by_city_tuple.update(zip(missing_city_tuple_list, city_item_ids))

    return item_id_by_city_tuple, item_id_by_district_tuple, item_id_by_country_id


def create_legal_status_items(
    legal_status_info_dict: Dict[str, Any],
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing legal status items as a dict legal status id -> legal status item id
    item_id_by_legal_status_id = {
        legal_status_id: legal_status_item_id
        for (
            legal_status_id,
            legal_status_item_id,
        ) in iter_legal_status_item_id_from_wikibase(wb_handle)
    }

    # Iterate on legal status info dict
    # Note: Too few legal status to write them using parallelism
    item_id_by_legal_status_label = {}
    for legal_status_id, legal_status_info in legal_status_info_dict.items():
        legal_status_label = legal_status_info["name"]
        if legal_status_id not in item_id_by_legal_status_id:
            log.debug(legal_status_label)
            item = build_legal_status_item(legal_status_info, wb_handle)

            item_id = wb_handle.write_item(
                item,
                legal_status_label,
                "legal status",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_legal_status_label[legal_status_label] = item_id

    return item_id_by_legal_status_label


def create_corporation_item(
    row,
    names_df: pd.DataFrame,
    juridical_status_df: Optional[pd.DataFrame],
    locations_df: Optional[pd.DataFrame],
    item_id_by_city_tuple: Dict[Tuple[str, str, str], str],
    item_id_by_district_tuple: Dict[Tuple[str, str], str],
    item_id_by_country_id: Dict[str, str],
    item_id_by_location_type_id: Dict[str, str],
    item_id_by_legal_status_label: Dict[str, str],
    context: Context,
) -> Tuple[str, str]:
    """Create or update a Wikibase item corresponding to a DFIH corporation."""

    wb_handle = context.wb_handle

    def year_qualifier(year: int, prop_nr: str):
        """Create WDTime instance from year value."""
        return WDTime(
            f"+0000000{year}-00-00T00:00:00Z", prop_nr, precision=9, is_qualifier=True
        )

    statements: List[Any] = [
        WDItemID(BaseItem.CORP, BaseProperty.INSTANCE_OF, references=dfih_references())
    ]

    corp_references = dfih_references()

    # statements from dfih_corporation_sd_ed
    statements.extend(
        [
            WDExternalID(
                str(row.Index),
                BaseProperty.DFIH_CORP_ID,
                references=corp_references,
            ),
            *date_qualifiers(
                row,
                is_qualifier=False,
                references=corp_references,
            ),
        ]
    )
    if not empty_string(row.PUBLIC_STATUS):
        statements.append(
            SafeWDString(
                row.PUBLIC_STATUS,
                BaseProperty.PUBLIC_STATUS,
                references=corp_references,
            )
        )

    # Name statements from dfih_corporation_names
    for name_row in names_df.itertuples():

        source_statements = (
            [SafeWDString(name_row.SOURCE, BaseProperty.SOURCE, is_reference=True)]
            if not empty_string(name_row.SOURCE)
            else None
        )

        statements.append(
            SafeWDString(
                name_row.NAME,
                BaseProperty.NAME,
                qualifiers=date_qualifiers(name_row, none_if_empty=True),
                references=dfih_references(additional_references=source_statements),
            )
        )

    # Juridical status statements from dfih_corporation_juridical_status
    if juridical_status_df is not None:
        for juridical_status_row in juridical_status_df.itertuples():

            legal_status_label = juridical_status_row.JURIDICAL_STATUS
            legal_status_item = item_id_by_legal_status_label.get(legal_status_label)
            if legal_status_item is None:
                continue

            # Date qualifiers
            qualifiers = date_qualifiers(juridical_status_row)

            # Comments qualifier
            comments_value = juridical_status_row.COMMENTS
            if comments_value:
                qualifiers.append(
                    SafeWDString(
                        comments_value, BaseProperty.COMMENTS, is_qualifier=True
                    )
                )

            # Source reference
            source_references = dfih_references(
                additional_references=(
                    [
                        SafeWDString(
                            juridical_status_row.SOURCE,
                            BaseProperty.STATED_IN,
                            is_reference=True,
                        )
                    ]
                    if not empty_string(juridical_status_row.SOURCE)
                    else None
                )
            )

            # Legal status
            legal_status_statement = WDItemID(
                legal_status_item,
                BaseProperty.CORP_LEGAL_STATUS,
                qualifiers=qualifiers,
                references=source_references,
            )
            statements.append(legal_status_statement)

    # Location statements from dfih_corporation_locations.csv
    if locations_df is not None:
        for location_row in locations_df.itertuples():

            location_type_id = str(location_row.LOCATION_TYPES_ID)
            if location_type_id not in item_id_by_location_type_id:
                log.error("Can't find location type %s", location_type_id)
                continue

            location_type_item_id = item_id_by_location_type_id.get(location_type_id)
            if location_type_item_id is None:
                log.error("Can't find location type item id for %s", location_type_id)
                continue

            qualifiers = []

            country_id = location_row.COUNTRY_ID
            city_tuple = (location_row.CITY_ID, location_row.DISTRICT_ID, country_id)
            district_tuple = (location_row.DISTRICT_ID, country_id)

            # city?
            if city_tuple in item_id_by_city_tuple:
                qualifiers.append(
                    WDItemID(
                        item_id_by_city_tuple[city_tuple],
                        BaseProperty.LOCATION_CITY,
                        is_qualifier=True,
                    )
                )

            # district?
            elif district_tuple in item_id_by_district_tuple:
                qualifiers.append(
                    WDItemID(
                        item_id_by_district_tuple[district_tuple],
                        BaseProperty.LOCATION_DISTRICT,
                        is_qualifier=True,
                    )
                )

            # country?
            elif country_id in item_id_by_country_id:
                qualifiers.append(
                    WDItemID(
                        item_id_by_country_id[country_id],
                        BaseProperty.LOCATION_COUNTRY,
                        is_qualifier=True,
                    )
                )

            if location_row.ADDRESS:
                qualifiers.append(
                    SafeWDString(
                        location_row.ADDRESS,
                        BaseProperty.LOCATION_ADDRESS,
                        is_qualifier=True,
                    )
                )

            if location_row.IN_YEARBOOK_FROM:
                qualifiers.append(
                    year_qualifier(
                        location_row.IN_YEARBOOK_FROM, BaseProperty.IN_YEARBOOK_FROM
                    )
                )
            if location_row.IN_YEARBOOK_TO:
                qualifiers.append(
                    year_qualifier(
                        location_row.IN_YEARBOOK_TO, BaseProperty.IN_YEARBOOK_TO
                    )
                )

            location_type_statement = WDItemID(
                location_type_item_id,
                BaseProperty.CORP_LOCATION_TYPE,
                qualifiers=qualifiers,
                references=dfih_references(),
            )
            statements.append(location_type_statement)

    corporation_label = find_latest_df_name(names_df)
    return (
        wb_handle.compute_item(
            statements,
            corporation_label,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.CORP},
        ),
        corporation_label,
    )


def compute_item_id_by_corp_id(wb_handle: WBHandle):
    """Compute corporation id to item id dict."""
    item_id_by_corp_id = wb_handle.id_mapper(BaseProperty.DFIH_CORP_ID)
    if item_id_by_corp_id:
        return {int(k): v for k, v in item_id_by_corp_id.items()}
    return {}


def create_corporation_items(
    corporation_sd_ed_df: pd.core.frame.DataFrame,
    corporation_names_df: pd.core.frame.DataFrame,
    corporation_juridical_status_df: pd.core.frame.DataFrame,
    corporation_locations_df: pd.core.frame.DataFrame,
    item_id_by_city_tuple: Dict[Tuple[str, str, str], str],
    item_id_by_district_tuple: Dict[Tuple[str, str], str],
    item_id_by_country_id: Dict[str, str],
    item_id_by_location_type_id: Dict[str, str],
    item_id_by_legal_status_label: Dict[str, str],
    context: Context,
):
    wb_handle = context.wb_handle

    # Compute corporation id to item id dict
    item_id_by_corp_id = compute_item_id_by_corp_id(wb_handle)

    # Compute missing corporation
    missing_corporation_rows = [
        row
        for row in corporation_sd_ed_df.itertuples()
        if row.Index not in item_id_by_corp_id
        and row.Index in corporation_names_df.index
    ]

    # Worker
    def create_corporation_worker(row):
        dfih_corporation_id = row.Index

        names_df = corporation_names_df.loc[[dfih_corporation_id]]

        locations_df = (
            corporation_locations_df.loc[[dfih_corporation_id]]
            if dfih_corporation_id in corporation_locations_df.index
            else None
        )
        if locations_df is None:
            log.debug(
                "No location information for corporation [%d]", dfih_corporation_id
            )

        juridical_status_df = (
            corporation_juridical_status_df.loc[[dfih_corporation_id]]
            if dfih_corporation_id in corporation_juridical_status_df.index
            else None
        )
        if juridical_status_df is None:
            log.debug(
                "No juridical status information for corporation [%d]",
                dfih_corporation_id,
            )

        item, item_label = create_corporation_item(
            row,
            names_df,
            juridical_status_df,
            locations_df,
            item_id_by_city_tuple,
            item_id_by_district_tuple,
            item_id_by_country_id,
            item_id_by_location_type_id,
            item_id_by_legal_status_label,
            context,
        )

        return wb_handle.write_item(
            item,
            dfih_corporation_id,
            "corporation",
            label=item_label,
        )

    # parallel create
    log.info("== CORPORATIONS ==")
    corporation_item_ids = parallel_item_process(
        create_corporation_worker,
        missing_corporation_rows,
        max_workers=context.max_workers,
    )
    item_id_by_corp_id.update(
        zip([row.Index for row in missing_corporation_rows], corporation_item_ids)
    )

    return item_id_by_corp_id


def create_position_item(wb_handle: WBHandle, job_id: str, job: str):
    """Create position item"""

    statements = [
        WDItemID(
            BaseItem.POSITION,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            job_id,
            BaseProperty.DFIH_JOB_ID,
            references=dfih_references(),
            qualifiers=[SafeWDString(job, BaseProperty.NAMED_AS, is_qualifier=True)],
        ),
    ]
    return wb_handle.compute_item(statements, job, item_label_lang=DFIH_LABEL_LANGUAGE)


def iter_position_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.POSITION}.
        ?item p:{BaseProperty.DFIH_JOB_ID} ?stmt {{
            ?item wdt:{BaseProperty.DFIH_JOB_ID} ?job_id.
            ?stmt prov:wasDerivedFrom ?refnode.
            ?refnode pr:{BaseProperty.SOURCE_DATABASE} wd:{BaseItem.DFIH_DATABASE}.
        }}
    }}
    """

    def value_func(result):
        return result["job_id"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def compute_item_id_by_job_id(wb_handle: WBHandle):
    """Get existing position items as a dict position job id -> position item id."""
    return {
        job_id: position_item_id
        for (job_id, position_item_id) in iter_position_item_id_from_wikibase(wb_handle)
    }


def create_position_items(
    person_function_df: pd.core.frame.DataFrame,
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing position items as a dict position job id -> position item id
    item_id_by_job_id = compute_item_id_by_job_id(wb_handle)

    # missing person function
    missing_person_function_tuple_list = sorted(
        (
            (row.JOB_ID, row.JOB)
            for row in person_function_df[["JOB_ID", "JOB"]]
            .drop_duplicates()
            .itertuples()
            if row.JOB_ID not in item_id_by_job_id
        ),
        key=itemgetter(1),
    )

    # Worker
    def create_person_function_worker(person_function_tuple):
        job_id, job = person_function_tuple
        item = create_position_item(wb_handle, job_id, job)
        return wb_handle.write_item(item, job, "position")

    log.info("== PERSON FUNCTIONS ==")
    position_item_ids = parallel_item_process(
        create_person_function_worker,
        missing_person_function_tuple_list,
        max_workers=context.max_workers,
    )
    item_id_by_job_id.update(
        zip([tup[0] for tup in missing_person_function_tuple_list], position_item_ids)
    )
    return item_id_by_job_id


def iter_person_function_statement(
    dfih_person_id: int,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_job_id: Dict[str, str],
):

    for function_row in person_function_df.itertuples():
        dfih_corporation_id = function_row.CORPORATION
        corporation_item_id = item_id_by_corporation_id.get(dfih_corporation_id)
        if not corporation_item_id:
            log.error(
                "Can't find corporation [%d] for person [%d]",
                dfih_corporation_id,
                dfih_person_id,
            )
            continue

        qualifiers = []
        job_id = function_row.JOB_ID
        if not job_id:
            log.error("No position id for person [%d]", dfih_person_id)
            continue

        position_item_id = item_id_by_job_id.get(job_id)
        if not position_item_id:
            log.error("Can't find position id '%s' item", job_id)
            continue

        qualifiers.append(
            WDItemID(
                position_item_id,
                BaseProperty.PERSON_FUNCTION,
                is_qualifier=True,
            )
        )
        qualifiers.extend(date_qualifiers(function_row))
        if function_row.COMMENTS:
            qualifiers.append(
                SafeWDString(
                    function_row.COMMENTS,
                    BaseProperty.COMMENTS,
                    is_qualifier=True,
                )
            )

        source_references = (
            [
                SafeWDString(
                    function_row.SOURCE,
                    BaseProperty.STATED_IN,
                    is_reference=True,
                )
            ]
            if not empty_string(function_row.SOURCE)
            else None
        )

        yield WDItemID(
            corporation_item_id,
            BaseProperty.PERSON_FUNCTION_CORPORATION,
            qualifiers=qualifiers,
            references=dfih_references(additional_references=source_references),
        )


def create_person_item(
    row,
    person_function_df: pd.DataFrame,
    item_id_by_corporation_id: Dict[int, str],
    item_id_by_job_id: Dict[str, str],
    wb_handle: WBHandle,
) -> Tuple[str, str]:

    # statements from persons.csv
    person_references = dfih_references(
        additional_references=(
            [SafeWDString(row.SOURCE, BaseProperty.STATED_IN, is_reference=True)]
            if not empty_string(row.SOURCE)
            else None
        )
    )

    dfih_person_id = row.Index
    statements = [
        WDItemID(
            BaseItem.PERSON,
            BaseProperty.INSTANCE_OF,
            references=person_references,
        ),
        WDExternalID(
            str(dfih_person_id),
            BaseProperty.DFIH_PERSON_ID,
            qualifiers=[
                SafeWDString(row.NAME, BaseProperty.NAMED_AS, is_qualifier=True)
            ],
            references=person_references,
        ),
    ]
    statements.extend(
        date_qualifiers(
            row,
            is_qualifier=False,
            references=person_references,
            start_date_property_id=BaseProperty.BIRTH_DATE,
            end_date_property_id=BaseProperty.DEATH_DATE,
        )
    )
    if not empty_string(row.COMMENTS):
        statements.append(
            SafeWDString(
                row.COMMENTS,
                BaseProperty.COMMENTS,
                references=person_references,
            )
        )

    # Function statements from person_function
    if person_function_df is not None:

        for function_statement in iter_person_function_statement(
            dfih_person_id,
            person_function_df,
            item_id_by_corporation_id,
            item_id_by_job_id,
        ):
            statements.append(function_statement)

    person_label = row.NAME
    return (
        wb_handle.compute_item(
            statements,
            person_label,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.PERSON},
        ),
        person_label,
    )


def iter_person_dfih_id_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.PERSON}.
        ?item wdt:{BaseProperty.DFIH_PERSON_ID} ?dfih_person_id
    }}
    """

    def value_func(result):
        return result["dfih_person_id"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def create_stock_item(
    wb_handle: WBHandle,
    stock_id,
    corporations_df: pd.DataFrame,
    names_df: pd.DataFrame,
    types_df: pd.DataFrame,
    codetype_df: pd.DataFrame,
    property_id_from_codetype: Dict[str, str],
    item_id_by_stock_group_id: Dict[int, str],
    item_id_by_corporation_id: Dict[int, str],
) -> Tuple[str, str]:

    stock_references = dfih_references()

    statements = [
        WDItemID(
            BaseItem.STOCK,
            BaseProperty.INSTANCE_OF,
            references=stock_references,
        ),
        WDExternalID(
            str(stock_id),
            BaseProperty.DFIH_STOCK_ID,
            references=stock_references,
        ),
    ]

    # Name statements from stock_names
    if names_df is not None:

        for name_row in names_df.itertuples():

            statements.append(
                SafeWDString(
                    name_row.NAME,
                    BaseProperty.NAME,
                    qualifiers=date_qualifiers(name_row, none_if_empty=True),
                    references=stock_references,
                )
            )

    # Corporation statements from stock_corporations
    if corporations_df is not None:

        for corp_row in corporations_df.itertuples():

            corp_item_id = item_id_by_corporation_id.get(corp_row.CORPORATION)
            if corp_item_id is None:
                log.warning("Unknown corporation [%d]", corp_row.CORPORATION)
                continue

            statements.append(
                WDItemID(
                    corp_item_id,
                    BaseProperty.STOCK_CORPORATION,
                    qualifiers=date_qualifiers(corp_row, none_if_empty=True),
                    references=stock_references,
                )
            )

    # type statements from stock_types
    if types_df is not None:

        for type_row in types_df.itertuples():

            stock_type_qualifiers = []

            # Exact type
            stock_type_name = type_row.TYPE

            if stock_type_name:
                stock_type_qualifiers.append(
                    SafeWDString(
                        stock_type_name,
                        BaseProperty.EXACT_STOCK_TYPE,
                        is_qualifier=True,
                    )
                )
            stock_type_qualifiers.extend(date_qualifiers(type_row))

            # Simplified type
            stock_group = (
                type_row.STOCK_GROUP
                if isinstance(type_row.STOCK_GROUP, str) and type_row.STOCK_GROUP != ""
                else None
            )
            stock_group_item_id = item_id_by_stock_group_id.get(stock_group)

            if stock_group and stock_group_item_id:
                statements.append(
                    WDItemID(
                        stock_group_item_id,
                        BaseProperty.SIMPLIFIED_STOCK_TYPE,
                        qualifiers=stock_type_qualifiers,
                        references=stock_references,
                    )
                )

    if codetype_df is not None:

        for codetype_row in codetype_df.itertuples():

            property_id = property_id_from_codetype.get(codetype_row.CODETYPE)
            if property_id is None:
                log.error("Unknown codetype [%s]", codetype_row.CODETYPE)
                continue

            statements.append(
                WDExternalID(
                    codetype_row.CODE,
                    property_id,
                    qualifiers=date_qualifiers(codetype_row, none_if_empty=True),
                    references=stock_references,
                )
            )

    stock_label = find_latest_df_name(names_df)
    return (
        wb_handle.compute_item(
            statements,
            stock_label,
            fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.STOCK},
        ),
        stock_label,
    )


def iter_stock_dfih_id_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.STOCK}.
        ?item wdt:{BaseProperty.DFIH_STOCK_ID} ?dfih_stock_id
    }}
    """

    def value_func(result):
        return result["dfih_stock_id"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def iter_stock_sector_id_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.DFIH_STOCK_SECTOR}.
        ?item wdt:{BaseProperty.DFIH_STOCK_SECTOR_ID} ?dfih_stock_sector_id
    }}
    """

    def value_func(result):
        return int(result["dfih_stock_sector_id"])

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def create_person_items(
    persons_df: pd.core.frame.DataFrame,
    person_function_df: pd.core.frame.DataFrame,
    item_id_by_corp_id: Dict[int, str],
    item_id_by_job_id: Dict[str, str],
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing person items as a dict dfih person id -> item id
    item_id_by_person_id = {
        int(dfih_person_id): person_item_id
        for (
            dfih_person_id,
            person_item_id,
        ) in iter_person_dfih_id_item_id_from_wikibase(wb_handle)
    }

    # Build missing person id list
    missing_person_row_list = [
        row
        for row in persons_df.sort_values(by=["NAME"]).itertuples()
        if row.Index not in item_id_by_person_id and row.NAME is not None
    ]

    # worker
    def create_person_worker(person_row):
        dfih_person_id = person_row.Index
        function_df = (
            person_function_df.loc[[dfih_person_id]]
            if dfih_person_id in person_function_df.index
            else None
        )
        item, item_label = create_person_item(
            person_row,
            function_df,
            item_id_by_corp_id,
            item_id_by_job_id,
            wb_handle,
        )
        return wb_handle.write_item(
            item,
            dfih_person_id,
            "person",
            label=item_label,
        )

    log.info("== PERSONS ==")
    parallel_item_process(
        create_person_worker, missing_person_row_list, max_workers=context.max_workers
    )


def create_stock_items(
    stock_corporation_df: pd.core.frame.DataFrame,
    stock_names_df: pd.core.frame.DataFrame,
    stock_types_df: pd.core.frame.DataFrame,
    stock_codetype_df: pd.core.frame.DataFrame,
    property_id_from_codetype: Dict[str, str],
    item_id_by_stock_group_id: Dict[int, str],
    item_id_by_corporation_id: Dict[int, str],
    context: Context,
):
    wb_handle = context.wb_handle

    # Get existing stock items as a dict dfih stock id -> item id
    item_id_by_stock_id = {
        int(dfih_stock_id): stock_item_id
        for (
            dfih_stock_id,
            stock_item_id,
        ) in iter_stock_dfih_id_item_id_from_wikibase(wb_handle)
    }

    # missing stock items
    missing_stock_tuple_list = [
        (stock_id, names_df)
        for stock_id, names_df in stock_names_df.groupby(["STOCK"])
        if stock_id not in item_id_by_stock_id
    ]

    # Worker
    def create_stock_worker(stock_tuple):
        stock_id, names_df = stock_tuple

        # Associated corporations
        corporations_df = (
            stock_corporation_df.loc[[stock_id]]
            if stock_id in stock_corporation_df.index
            else None
        )

        # Associated types
        types_df = (
            stock_types_df.loc[[stock_id]] if stock_id in stock_types_df.index else None
        )

        # Associated codetype
        codetype_df = (
            stock_codetype_df.loc[[stock_id]]
            if stock_id in stock_codetype_df.index
            else None
        )

        item, item_label = create_stock_item(
            wb_handle,
            stock_id,
            corporations_df,
            names_df,
            types_df,
            codetype_df,
            property_id_from_codetype,
            item_id_by_stock_group_id,
            item_id_by_corporation_id,
        )

        stock_item_id = wb_handle.write_item(
            item,
            stock_id,
            "stock",
            label=item_label,
        )
        return stock_item_id

    # Let's create stocks!
    log.info("== STOCKS ==")
    parallel_item_process(
        create_stock_worker, missing_stock_tuple_list, max_workers=context.max_workers
    )


def load_locations_types_csv(csv_file: Path):
    """Load DFIH location types info CSV file and return a dict."""

    def str_to_list(value_str):
        return [val.strip() for val in value_str.split(",") if val.strip() != ""]

    location_type_info_dict = {}
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            s_row = {k: v.strip() for k, v in row.items()}
            type_id = s_row["DFIH_LOCATION_TYPES_ID"]
            location_type_info_dict[type_id] = {
                "code": type_id,
                "name": s_row["DFIH_LOCATION_TYPES"],
                "labels": {
                    "en": s_row["label_en"],
                    "fr": s_row["label_fr"],
                },
                "description": {
                    "en": s_row["description_en"],
                    "fr": s_row["description_fr"],
                },
                "aliases": {
                    "en": str_to_list(s_row["aliases_en"]),
                    "fr": str_to_list(s_row["aliases_fr"]),
                },
            }
    return location_type_info_dict


def load_legal_status_csv(csv_file: Path):
    """Load DFIH legal status info CSV file and return a dict."""

    def str_to_list(value_str):
        return [val.strip() for val in value_str.split(",") if val.strip() != ""]

    legal_status_info_dict = {}
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            s_row = {k: v.strip() for k, v in row.items()}
            type_id = s_row["DFIH_LEGAL_STATUS_ID"]
            legal_status_info_dict[type_id] = {
                "code": type_id,
                "name": s_row["DFIH_LEGAL_STATUS"],
                "labels": {
                    "en": s_row["label_en"],
                    "fr": s_row["label_fr"],
                },
                "aliases": {
                    "en": str_to_list(s_row["aliases_en"]),
                    "fr": str_to_list(s_row["aliases_fr"]),
                },
            }
    return legal_status_info_dict


def load_stock_types_csv(csv_file: Path):
    """Load DFIH stock type info CSV file and return a dict."""

    stock_type_info_dict = {}
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            s_row = {k: v.strip() for k, v in row.items()}
            type_id = s_row["DFIH_STOCK_GROUP_ID"]
            stock_type_info_dict[type_id] = {
                "code": type_id,
                "labels": {
                    "en": s_row["label_en"],
                    "fr": s_row["label_fr"],
                },
            }
    return stock_type_info_dict


def load_stock_codetypes_csv(csv_file: Path):
    """Load stock code types info CSV file and return a dict."""

    def str_to_list(value_str):
        return [val.strip() for val in value_str.split(",") if val.strip() != ""]

    stock_codetypes_info_dict = {}
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            s_row = {k: v.strip() for k, v in row.items()}
            name = s_row["NAME"]
            stock_codetypes_info_dict[name] = {
                "name": name,
                "labels": {lg: s_row[f"label_{lg}"] for lg in ("en", "fr", "nl")},
                "aliases": {
                    lg: str_to_list(s_row[f"aliases_{lg}"]) for lg in ("en", "fr", "nl")
                },
            }
    return stock_codetypes_info_dict


def iter_legal_status_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LEGAL_STATUS}.
        ?item wdt:{BaseProperty.DFIH_LEGAL_STATUS_ID} ?legal_status_id
    }}
"""

    def value_func(result):
        return result["legal_status_id"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def build_legal_status_item(legal_status_info: Dict[str, Any], wb_handle: WBHandle):
    "Build item from legal status information."

    statements = [
        WDItemID(
            BaseItem.LEGAL_STATUS,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            legal_status_info["code"],
            BaseProperty.DFIH_LEGAL_STATUS_ID,
            references=dfih_references(),
            qualifiers=[
                SafeWDString(
                    legal_status_info["name"],
                    BaseProperty.NAMED_AS,
                    is_qualifier=True,
                ),
            ],
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (label, lang) for lang, label in legal_status_info["labels"].items()
        ],
        multilang_aliases=[
            (aliases, lang) for lang, aliases in legal_status_info["aliases"].items()
        ],
    )


def iter_location_type_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.LOCATION_TYPE}.
        ?item wdt:{BaseProperty.DFIH_LOCATION_TYPE_ID} ?loc_type_id
    }}
"""

    def value_func(result):
        return result["loc_type_id"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def build_location_type_item(location_type_info: Dict[str, Any], wb_handle: WBHandle):
    "Build item from location type information."

    statements = [
        WDItemID(
            BaseItem.LOCATION_TYPE,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            location_type_info["code"],
            BaseProperty.DFIH_LOCATION_TYPE_ID,
            references=dfih_references(),
            qualifiers=[
                SafeWDString(
                    location_type_info["name"],
                    BaseProperty.NAMED_AS,
                    is_qualifier=True,
                ),
            ],
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (label, lang) for lang, label in location_type_info["labels"].items()
        ],
        multilang_descriptions=[
            ("(from DFIH)" if not description else f"{description} (from DFIH)", lang)
            for lang, description in location_type_info["description"].items()
        ],
        multilang_aliases=[
            (aliases, lang) for lang, aliases in location_type_info["aliases"].items()
        ],
    )


def create_location_type_items(
    location_type_info_dict: Dict[str, Any],
    context: Context,
) -> Dict[str, str]:

    wb_handle = context.wb_handle

    # Get existing location type items as a dict (location type id) -> location type item id
    item_id_by_location_type_id = {
        location_type_id: location_type_item_id
        for (
            location_type_id,
            location_type_item_id,
        ) in iter_location_type_item_id_from_wikibase(wb_handle)
    }

    # Create missing location type items and complete dict
    for type_id, type_info in location_type_info_dict.items():
        if type_id not in item_id_by_location_type_id:
            location_type_item = build_location_type_item(type_info, wb_handle)

            item_id = wb_handle.write_item(
                location_type_item,
                type_info["name"],
                "location type",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_location_type_id[type_id] = item_id

    return item_id_by_location_type_id


def iter_stock_type_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.SIMPLIFIED_STOCK_TYPE}.
        ?item wdt:{BaseProperty.DFIH_STOCK_GROUP_ID} ?stock_group_id
    }}
"""

    def value_func(result):
        return result["stock_group_id"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def build_stock_type_item(stock_type_info: Dict[str, Any], wb_handle: WBHandle):
    "Build item from stock type information."

    statements = [
        WDItemID(
            BaseItem.SIMPLIFIED_STOCK_TYPE,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        WDExternalID(
            stock_type_info["code"],
            BaseProperty.DFIH_STOCK_GROUP_ID,
            references=dfih_references(),
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (label, lang) for lang, label in stock_type_info["labels"].items()
        ],
    )


def create_stock_type_items(
    stock_type_info_dict: Dict[str, Any],
    context: Context,
) -> Dict[int, str]:

    wb_handle = context.wb_handle

    # Get existing stock type items as a dict (stock group id) -> stock type item id
    item_id_by_stock_group_id = {
        stock_group_id: stock_type_item_id
        for (
            stock_group_id,
            stock_type_item_id,
        ) in iter_stock_type_item_id_from_wikibase(wb_handle)
    }

    # Create missing stock type items and complete dict
    # Note: too few items to be created using parallelism
    for group_id, type_info in stock_type_info_dict.items():
        if group_id not in item_id_by_stock_group_id:
            stock_type_item = build_stock_type_item(type_info, wb_handle)

            item_id = wb_handle.write_item(
                stock_type_item,
                type_info["code"],
                "stock type",
                check_if_necessary=False,
            )
            if item_id:
                item_id_by_stock_group_id[group_id] = item_id

    return item_id_by_stock_group_id


def iter_stock_codetype_item_id_from_wikibase(wb_handle: WBHandle):
    query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.STOCK_CODETYPE}.
        ?item wdt:{BaseProperty.NAME} ?stock_codetype_name
    }}
"""

    def value_func(result):
        return result["stock_codetype_name"]

    return wb_handle.iter_info_item_id_from_wikibase(query, value_func)


def build_stock_codetype_item(stock_codetype_info: Dict[str, Any], wb_handle: WBHandle):
    "Build item from stock code type information."

    statements = [
        WDItemID(
            BaseItem.STOCK_CODETYPE,
            BaseProperty.INSTANCE_OF,
            references=dfih_references(),
        ),
        SafeWDString(
            stock_codetype_info["name"],
            BaseProperty.NAME,
            references=dfih_references(),
        ),
    ]

    return wb_handle.build_multilang_item(
        statements,
        multilang_labels=[
            (label, lang) for lang, label in stock_codetype_info["labels"].items()
        ],
        multilang_aliases=[
            (aliases, lang) for lang, aliases in stock_codetype_info["aliases"].items()
        ],
    )


def create_stock_sector_item(
    stock_sector_id: int,
    stock_sector_label: str,
    stock_sector_name_list: List[str],
    wb_handle: WBHandle,
):
    """Create stock sector item."""

    stock_sector_references = dfih_references()

    statements = [
        WDItemID(
            BaseItem.DFIH_STOCK_SECTOR,
            BaseProperty.INSTANCE_OF,
            references=stock_sector_references,
        ),
        WDExternalID(
            str(stock_sector_id),
            BaseProperty.DFIH_STOCK_SECTOR_ID,
            references=stock_sector_references,
        ),
    ]
    for stock_sector_name in stock_sector_name_list:
        statements.append(
            SafeWDString(
                stock_sector_name, BaseProperty.NAME, references=stock_sector_references
            )
        )

    stock_sector_item = wb_handle.compute_item(
        statements,
        stock_sector_label,
        fast_run_base_filter={BaseProperty.INSTANCE_OF: BaseItem.DFIH_STOCK_SECTOR},
    )
    return wb_handle.write_item(
        stock_sector_item,
        stock_sector_id,
        "stock sector",
        label=stock_sector_label,
    )


def build_item_id_by_stock_sector_id(context: Context):
    return {
        stock_sector_id: item_id
        for (stock_sector_id, item_id) in iter_stock_sector_id_item_id_from_wikibase(
            context.wb_handle
        )
    }


def create_stock_sector_items(stock_spot_prices_df: pd.DataFrame, context: Context):
    """Create stock sector items."""

    wb_handle = context.wb_handle

    # First get existing sectors in WikiBase
    item_id_by_stock_sector_id: Dict[int, str] = build_item_id_by_stock_sector_id(
        context
    )

    # Compute missing stock_sector items
    missing_stock_sector_id_list: List[int] = [
        stock_sector_id
        for stock_sector_id in stock_spot_prices_df["SECTOR"].unique().tolist()
        if stock_sector_id not in item_id_by_stock_sector_id
    ]

    # Create sector item worker
    def create_stock_sector_worker(stock_sector_id: int):
        stock_sector_name_list = (
            stock_spot_prices_df[stock_spot_prices_df["SECTOR"] == stock_sector_id][
                "SECTOR_NAME"
            ]
            .unique()
            .tolist()
        )
        stock_sector_label = stock_sector_name_list[-1]

        return create_stock_sector_item(
            stock_sector_id,
            stock_sector_label,
            stock_sector_name_list,
            wb_handle,
        )

    log.info("== STOCK SECTORS ==")
    stock_sector_item_ids = parallel_item_process(
        create_stock_sector_worker,
        missing_stock_sector_id_list,
        max_workers=context.max_workers,
    )
    item_id_by_stock_sector_id.update(
        zip(missing_stock_sector_id_list, stock_sector_item_ids)
    )

    return item_id_by_stock_sector_id


def find_stock_item_by_dfih_stock_id(stock_id: int, wb_handle: WBHandle) -> str:
    sparql_query = f"""SELECT *
    WHERE
    {{
        ?item wdt:{BaseProperty.INSTANCE_OF} wd:{BaseItem.STOCK}.
        ?item wdt:{BaseProperty.DFIH_STOCK_ID} "{stock_id}"
    }}
    """
    response = wb_handle.item_engine.execute_sparql_query(
        sparql_query, endpoint=wb_handle.sparql_endpoint_url
    )

    if "results" not in response:
        raise ValueError(f"SPARQL query error: {sparql_query}")

    results = response["results"].get("bindings", [])
    return itemid_from_url(results[0]["item"]["value"]) if results else None


def update_stock_item_with_spot_prices(
    stock_item_id: str,
    stock_spot_prices_df: pd.DataFrame,  # already filtered for the given stock
    item_id_by_stock_sector_id: Dict[int, str],
    context: Context,
):
    """Update stock item, adding spot prices information."""

    log.info("Updating item...")

    spot_prices_references = dfih_references()
    statements = []

    for tuple in stock_spot_prices_df.itertuples():

        if np.isnan(tuple.STOCKEXCHANGE_ID):
            continue
        stock_exchange_id = int(tuple.STOCKEXCHANGE_ID)
        spot_price_property = (
            BaseProperty.PARIS_OFFICIAL_MARKET_STOCK_PRICE
            if stock_exchange_id == 1
            else BaseProperty.PARIS_OVER_THE_COUNTER_MARKET_STOCK_PRICE
        )

        qualifiers = [
            WDTime(
                format_wikibase_date(tuple.DAY),
                BaseProperty.TRADING_DAY,
                is_qualifier=True,
            ),
            WDExternalID(
                str(tuple.NOTATION),
                BaseProperty.DFIH_STOCK_NOTATION_ID,
                is_qualifier=True,
            ),
        ]
        sector_item_id = item_id_by_stock_sector_id.get(tuple.SECTOR)
        if sector_item_id:
            qualifiers.append(
                WDItemID(
                    sector_item_id,
                    BaseProperty.DFIH_STOCK_SECTOR,
                    is_qualifier=True,
                )
            )

        statements.append(
            WDQuantity(
                tuple.PRICE,
                spot_price_property,
                qualifiers=qualifiers,
                references=spot_prices_references,
            )
        )

    wb_handle = context.wb_handle
    item = wb_handle.item_engine(wd_item_id=stock_item_id, data=statements)
    item.write(wb_handle.login_instance)

    log.info("Done. %d spot price(s) added", len(statements))


def import_csv_data(
    csv_files_dir: Path,
    context: Context,
):
    """Import dfih entities from CSV data."""

    # Here default language parameter value is ignored
    # in favour of DFIH_LABEL_LANGUAGE value

    # Load CSV sources
    log.info("Loading dataframes...")
    corporation_sd_ed_df = load_corporations(
        csv_files_dir / "dfih_corporation_sd_ed.csv"
    )
    corporation_names_df = load_corporation_names(
        csv_files_dir / "dfih_corporation_names.csv"
    )
    corporation_locations_df = load_corporation_locations(
        csv_files_dir / "dfih_corporation_locations.csv"
    )
    corporation_juridical_status_df = load_corporation_juridical_status(
        csv_files_dir / "dfih_corporation_juridical_status.csv"
    )
    persons_df = load_persons(csv_files_dir / "dfih_persons.csv")
    person_function_df = load_person_function(
        csv_files_dir / "dfih_person_function.csv"
    )
    stock_corporation_df = load_stock_corporation(
        csv_files_dir / "dfih_stock_corporation.csv"
    )
    stock_spot_prices_df = load_stock_spot_prices(csv_files_dir / "prix_comptant.csv")
    log.debug("done.")

    # Load CSV files from "resources" dir, containing hand-crafted data,
    # not exported from DB.
    stock_names_df = load_stock_names(csv_files_dir / "dfih_stock_names.csv")
    stock_types_df = load_stock_types(csv_files_dir / "dfih_stock_types.csv")
    stock_codetype_df = load_stock_codetype(csv_files_dir / "dfih_stock_codetype.csv")

    # Create items in wikibase

    # Create city items (countries, districts, cities)
    (
        item_id_by_city_tuple,
        item_id_by_district_tuple,
        item_id_by_country_id,
    ) = create_country_district_city_items(corporation_locations_df, context)

    # Load dfih_location_types.csv
    location_types_info_dict = load_locations_types_csv(
        PROJECT_DIR / "resources" / "dfih_location_types.csv"
    )

    # Create location type items
    item_id_by_location_type_id = create_location_type_items(
        location_types_info_dict, context
    )

    # load dfih_legal_status.csv
    legal_status_info_dict = load_legal_status_csv(
        PROJECT_DIR / "resources" / "dfih_legal_status.csv"
    )

    # Create legal status
    item_id_by_legal_status_label = create_legal_status_items(
        legal_status_info_dict,
        context,
    )

    # Create corporation items
    item_id_by_corp_id = create_corporation_items(
        corporation_sd_ed_df,
        corporation_names_df,
        corporation_juridical_status_df,
        corporation_locations_df,
        item_id_by_city_tuple,
        item_id_by_district_tuple,
        item_id_by_country_id,
        item_id_by_location_type_id,
        item_id_by_legal_status_label,
        context,
    )

    # Create person position items
    item_id_by_job_id = create_position_items(person_function_df, context)

    # Create person items
    create_person_items(
        persons_df,
        person_function_df,
        item_id_by_corp_id,
        item_id_by_job_id,
        context,
    )

    # Load dfih_stock_types CSV file
    stock_types_info_dict = load_stock_types_csv(
        PROJECT_DIR / "resources" / "dfih_stock_types.csv"
    )

    # Create stock type items
    item_id_by_stock_group_id = create_stock_type_items(
        stock_types_info_dict,
        context,
    )

    property_id_from_codetype = {
        "CCDVT (SICOVAM) (CCDVT (1/3/1943) (1/3/1950: SICOVAM)": BaseProperty.CCDVT_STOCK_ID,
        "ISIN": BaseProperty.ISIN_STOCK_ID,
        "RGA": BaseProperty.RGA_STOCK_ID,
    }

    # create stock items
    create_stock_items(
        stock_corporation_df,
        stock_names_df,
        stock_types_df,
        stock_codetype_df,
        property_id_from_codetype,
        item_id_by_stock_group_id,
        item_id_by_corp_id,
        context,
    )

    # create sectors
    create_stock_sector_items(stock_spot_prices_df, context)

    log.info("done")


def add_spot_prices_to_stock(
    spot_prices_csv_file: Path,
    dfih_stock_id: int,
    context: Context,
):
    stock_item_id = find_stock_item_by_dfih_stock_id(dfih_stock_id, context.wb_handle)
    if stock_item_id is None:
        log.error(f"Can't find stock item from dfih stock id: {dfih_stock_id}")
        raise typer.Abort()
    log.info("Stock item found: %s", stock_item_id)

    log.info("Loading spot prices dataframe...")
    stock_spot_prices_df = load_stock_spot_prices(spot_prices_csv_file)
    stock_spot_prices_df = stock_spot_prices_df[
        stock_spot_prices_df["STOCK"] == dfih_stock_id
    ]
    log.info("done")

    item_id_by_stock_sector_id: Dict[int, str] = build_item_id_by_stock_sector_id(
        context
    )

    update_stock_item_with_spot_prices(
        stock_item_id, stock_spot_prices_df, item_id_by_stock_sector_id, context
    )


def update_person_functions(
    id_name_person_list: List[Tuple[int, str]], csv_files_dir: Path, context: Context
):
    # Load person functions
    person_function_df = load_person_function(
        csv_files_dir / "dfih_person_function.csv"
    )

    wb_handle = context.wb_handle

    # Get existing person items as a dict dfih person id -> item id
    item_id_by_person_id = {
        int(dfih_person_id): person_item_id
        for (
            dfih_person_id,
            person_item_id,
        ) in iter_person_dfih_id_item_id_from_wikibase(wb_handle)
    }
    item_id_by_corporation_id = compute_item_id_by_corp_id(wb_handle)
    item_id_by_job_id = compute_item_id_by_job_id(wb_handle)

    person_tuples_to_handle = [
        (person_id, person_name)
        for (person_id, person_name) in id_name_person_list
        if person_id in item_id_by_person_id and person_id in person_function_df.index
    ]

    def update_person_worker(person_tuple: Tuple[int, str]):
        person_id, person_name = person_tuple
        person_item_id = item_id_by_person_id.get(person_id)
        function_df = person_function_df.loc[[person_id]]
        log.debug("Updating %r (%s)", person_name, person_item_id)
        statements = [
            stmt
            for stmt in iter_person_function_statement(
                person_id,
                function_df,
                item_id_by_corporation_id,
                item_id_by_job_id,
            )
        ]
        item = wb_handle.item_engine(wd_item_id=person_item_id, data=statements)
        item.write(wb_handle.login_instance)

    parallel_item_process(
        update_person_worker, person_tuples_to_handle, max_workers=context.max_workers
    )
