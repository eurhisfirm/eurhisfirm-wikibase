FROM python:3.8

ENV PIP_NO_CACHE_DIR 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
RUN pip install -e .


ENTRYPOINT ["eurhisfirm-wikibase"]