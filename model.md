# Eurhisfirm WikiBase Model

- all items share a INSTANCE_OF statement
- statements have two mandatory references:
  - SOURCE_DATABASE
  - RETRIEVED

### COUNTRY

e.g. "France"

- DFIH_COUNTRY_ID (DFIH only)
  - qualifier NAMED_AS
- NAME (SCOB only)

### DISTRICT (DFIH only)

e.g. "Dordogne"

- LOCATION_COUNTRY (optional)
- DFIH_DISTRICT_ID
  - qualifier NAMED_AS

### CITY

e.g. "Bruxelles"

- LOCATION_COUNTRY (optional)
- LOCATION_DISTRICT (optional, DFIH only)
- NAME (SCOB only)
- DFIH_CITY_ID (DFIH only)
  - qualifier NAMED_AS

### LOCATION_TYPE

e.g. "head office" or "factory"

- DFIH_LOCATION_TYPE_ID (DFIH only)
  - qualifier NAMED_AS
- SCOB_LOCATION_TYPE_ID (SCOB only)
  - qualifier NAMED_AS
- SAID_TO_BE_THE_SAME_AS (optional, SCOB only)

### LEGAL_STATUS

e.g. "association" or "limited company (under Algerian law)"

- DFIH_LEGAL_STATUS_ID (DFIH only)
  - qualifier NAMED_AS
- SCOB_LEGAL_STATUS_ID (SCOB only)
  - qualifier NAMED_AS

### POSITION

e.g. "Administrateur délégué" or "Gouverneur"

- DFIH_JOB_ID (DFIH only)
  - qualifier NAMED_AS
- POSITION_TITLE (SCOB only)

### CORP

e.g. "Forges du Bas-Rhin" or "Assurances de Bordeaux (Transport Mer et Terre)"

- DFIH_CORP_ID (DFIH only)
  - qualifier optional dates
- SCOB_CORP_ID (SCOB only)
  - qualifier optional dates
- PUBLIC_STATUS (DFIH only)
- NAME
  - qualifier optional dates
  - reference optional SOURCE (DFIH only)
- CORP_LEGAL_STATUS
  - qualifier dates
  - qualifier optional COMMENTS
  - reference optional STATED_IN
- CORP_LOCATION_TYPE
  - qualifier optional LOCATION_CITY
  - qualifier optional LOCATION_DISTRICT (DFIH only)
  - qualifier optional LOCATION_COUNTRY
  - qualifier optional LOCATION_ADDRESS
  - qualifier optional IN_YEARBOOK_FROM (DFIH only)
  - qualifier optional IN_YEARBOOK_TO (DFIH only)

### PERSON

e.g. "prince Czartoryski de Lauriston-Boubers" or "André Mallet"

- DFIH_PERSON_ID (DFIH only)
  - qualifier NAMED_AS
- PERSON_GENDER (SCOB only)
- BIRTH_DATE
- DEATH_DATE
- COMMENTS
- PERSON_FUNCTION_CORPORATION
  - qualifier PERSON_FUNCTION
  - qualifier optional COMMENTS
  - reference optional STATED_IN

### STOCK

e.g. "S.N.C.F. Société Nationale des Chemins de fer Français 2,5%, type anc., r. 5000 f. (Jce 1/10/49). 1950-57"

- DFIH_STOCK_ID (DFIH only)
- SCOB_STOCK_ID (SCOB only)
- NAME
  - qualifier optional dates
- STOCK_CORPORATION
  - qualifier optional dates
- SIMPLIFIED_STOCK_TYPE (DFIH only)
  - qualifier EXACT_STOCK_TYPE
  - qualifier optional dates
- STOCK_TYPE (SCOB only)
  - qualifier compref???
  - qualifier optional dates
- STOCK_CODETYPE (SCOB only)
  - qualifier STOCK_CODE
  - qualifier optional dates
- CCDVT_STOCK_ID
- RGA_STOCK_ID
- ISIN_STOCK_ID

### STOCK_CODETYPE

e.g. "CCDVT", "RGA" or "ISIN" (for DFIH)
e.g. "SRW (SVM) Code (Belgian National Code)", "Audiotex Code", ... for SCOB

- alias: "SICOVAM" for CCDVT (for DFIH)
- NAME: string as found in csv-db-exports ("CCDVT (SICOVAM) (CCDVT (1/3/1943) (1/3/1950: SICOVAM)", "RGA" or "ISIN") (for DFIH)
- STOCK_CODETYPE_ID (for SCOB)

### SIMPLIFIED_STOCK_TYPE (DFIH only)

e.g. "Bond" (B) or "Share" (S)

- DFIH_STOCK_GROUP_ID

### STOCK_TYPE (SCOB only)

e.g. "Obligation participante" (1048)

- SCOB_STOCK_TYPE_ID
