#!/usr/bin/env python
import logging
import os
import random
from time import sleep
from typing import Dict

import dotenv
import pytest
from wikidataintegrator.wdi_core import WDExternalID, WDString

from base_entities_gen import BaseProperty
from eurhisfirm_wikibase.wdi_utils import WBHandle

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def wb_handle():
    def env(name):
        assert os.environ.get(name) is not None, name
        return os.environ[name]

    random.seed()
    dotenv.load_dotenv()

    wikibase_api_url: str = env("WIKIBASE_API_URL")
    mediawiki_bot_username: str = env("MEDIAWIKI_BOT_USERNAME")
    mediawiki_bot_password: str = env("MEDIAWIKI_BOT_PASSWORD")
    sparql_endpoint_url: str = env("SPARQL_ENDPOINT_URL")
    default_language: str = env("DEFAULT_LANGUAGE")
    available_languages_str: str = env("AVAILABLE_LANGUAGES")

    return WBHandle(
        wikibase_api_url,
        mediawiki_bot_username,
        mediawiki_bot_password,
        sparql_endpoint_url,
        default_language,
        available_languages_str,
    )


def test_retrieve(wb_handle):
    def search_string(search_string):
        results = wb_handle.item_engine.get_wd_search_results(
            search_string=search_string,
            mediawiki_api_url=wb_handle.mediawiki_api_url,
            language=wb_handle.default_language,
            dict_id_all_info=True,
        )
        assert len(results) != 0, f"{search_string!r} not found"

    search_string("Ponts")
    search_string("entreprise")
    search_string("Theatre de Niort")

    # Lines below return no results.
    # search_string("Société Conservatrice de l'Ecole Secondaire")
    # search_string("Fonderies de Romilly")


NAME_PROPERTY = BaseProperty.NAME
NAME_INIT_VAL = "foo name"
NAMED_AS_PROPERTY = BaseProperty.NAMED_AS
NAMED_AS_INIT_VAL = "foo named as"
POSITION_TITLE_PROPERTY = BaseProperty.POSITION_TITLE
POSITION_TITLE_VAL = "Mr"


@pytest.fixture
def create_remove_item(wb_handle):
    ITEM_LABEL = "bot_update_test_item"

    external_id = str(random.randint(1, 10000))

    statements = [
        WDString(NAME_INIT_VAL, NAME_PROPERTY),
        WDString(NAMED_AS_INIT_VAL, NAMED_AS_PROPERTY),
        WDExternalID(external_id, BaseProperty.DFIH_CORP_ID),
    ]

    item = wb_handle.item_engine(new_item=True, data=statements)
    item.set_label(ITEM_LABEL, wb_handle.default_language)
    item_id = item.write(wb_handle.login_instance)

    yield item_id, external_id

    # Item deletion
    wb_handle.item_engine.delete_item(
        item_id,
        "a good reason",
        wb_handle.login_instance,
        mediawiki_api_url=wb_handle.mediawiki_api_url,
    )


def test_minimal_create_remove(caplog, create_remove_item, wb_handle):

    caplog.set_level(logging.INFO, logger=__name__)
    item_id, _ = create_remove_item
    log.error("Create and remove item %r", item_id)


def get_item_by_external_id(wb_handle: WBHandle, external_id: str) -> Dict:
    query_tpl = """
    SELECT * WHERE {
        ?item wdt:<DFIH_CORP_ID> "<EXTERNAL_ID>";
            wdt:<NAME_PROPERTY> ?name;
            wdt:<NAMED_AS_PROPERTY> ?namedas.
        OPTIONAL { ?item wdt:<POSITION_TITLE_PROPERTY> ?postitle. }
    }
"""
    replace_list = [
        ("<DFIH_CORP_ID>", BaseProperty.DFIH_CORP_ID),
        ("<EXTERNAL_ID>", external_id),
        ("<NAME_PROPERTY>", NAME_PROPERTY),
        ("<NAMED_AS_PROPERTY>", NAMED_AS_PROPERTY),
        ("<POSITION_TITLE_PROPERTY>", POSITION_TITLE_PROPERTY),
        ("\n", " "),
    ]

    query = query_tpl
    for replace in replace_list:
        query = query.replace(*replace)

    return wb_handle.item_engine.execute_sparql_query(
        query.strip(), endpoint=wb_handle.sparql_endpoint_url
    )


def test_empty_update(create_remove_item, wb_handle: WBHandle):

    item_id, external_id = create_remove_item

    # Add empty statement list
    item = wb_handle.item_engine(wd_item_id=item_id, data=[])
    item.write(wb_handle.login_instance)

    # Created item needs some time to be seen by sparql engine :-(
    while True:
        results = get_item_by_external_id(wb_handle, external_id)
        if results["results"]["bindings"]:
            break
        sleep(1)

    # Checks that property values haven't changed
    bindings = results["results"]["bindings"]
    assert len(bindings) == 1
    assert bindings[0]["name"]["value"] == NAME_INIT_VAL
    assert bindings[0]["namedas"]["value"] == NAMED_AS_INIT_VAL


def test_existing_property_single_update(create_remove_item, wb_handle: WBHandle):

    item_id, external_id = create_remove_item

    # Add one existing property value statement
    NEW_NAME = "bar name"
    statements = [WDString(NEW_NAME, NAME_PROPERTY)]
    item = wb_handle.item_engine(wd_item_id=item_id, data=statements)
    item.write(wb_handle.login_instance)

    # Created item needs some time to be seen by sparql engine :-(
    while True:
        results = get_item_by_external_id(wb_handle, external_id)
        if results["results"]["bindings"]:
            break
        sleep(1)

    # Checks that name property value has changed
    bindings = results["results"]["bindings"]
    assert len(bindings) == 1
    assert bindings[0]["name"]["value"] == NEW_NAME
    assert bindings[0]["namedas"]["value"] == NAMED_AS_INIT_VAL


def test_existing_property_multi_update(create_remove_item, wb_handle: WBHandle):

    item_id, external_id = create_remove_item

    # Add one existing property value statement
    NEW_NAMES = [
        "bar name 1",
        "bar name 2",
        "bar name 3",
    ]
    statements = [WDString(name, NAME_PROPERTY) for name in NEW_NAMES]
    item = wb_handle.item_engine(wd_item_id=item_id, data=statements)
    item.write(wb_handle.login_instance)

    # Created item needs some time to be seen by sparql engine :-(
    while True:
        results = get_item_by_external_id(wb_handle, external_id)
        if results["results"]["bindings"]:
            break
        sleep(1)

    # Checks that name property value has changed
    bindings = results["results"]["bindings"]
    assert len(bindings) == len(NEW_NAMES)
    for i, binding in enumerate(bindings):
        assert binding["name"]["value"] == NEW_NAMES[i]
        assert binding["namedas"]["value"] == NAMED_AS_INIT_VAL


def test_new_property_update(create_remove_item, wb_handle: WBHandle):

    item_id, external_id = create_remove_item

    statements = [WDString(POSITION_TITLE_VAL, POSITION_TITLE_PROPERTY)]
    item = wb_handle.item_engine(wd_item_id=item_id, data=statements)
    item.write(wb_handle.login_instance)

    # Created item needs some time to be seen by sparql engine :-(
    while True:
        results = get_item_by_external_id(wb_handle, external_id)
        if results["results"]["bindings"]:
            break
        sleep(1)

    # Checks that new position title exists
    bindings = results["results"]["bindings"]
    assert len(bindings) == 1
    assert bindings[0]["name"]["value"] == NAME_INIT_VAL
    assert bindings[0]["namedas"]["value"] == NAMED_AS_INIT_VAL
    assert bindings[0]["postitle"]["value"] == POSITION_TITLE_VAL
