#!/bin/bash
#
# Import data from CSV files to Wikibase using docker
#
CONTAINER_NAME=eurhisfirm-wikibase:latest
BASE_ENTITIES_PY_FILE=base_entities_gen.py

if [ $# -ne 3 ]; then
    echo "$0 <envfile> <provider> <db_csv_dir>"
    exit 1
fi
ENV_FILE=$1
PROVIDER=$2
DB_CSV_DIR=$3

if [ ! -f $ENV_FILE ]; then
    echo "Environnement file not found"
fi

if [ "$PROVIDER" != "scob" -a "$PROVIDER" != "dfih" ]; then
    echo "Unknown provider: $PROVIDER, intended names are 'scob' and 'dfih'"
    exit 1
fi

if [ ! -d $DB_CSV_DIR ]; then
    echo "CSV dir not found"
    exit 1
fi

# Gets base_entities_gen.py if it doesn't exist
if [ ! -f $PWD/$BASE_ENTITIES_PY_FILE ]; then
    docker run --env-file $ENV_FILE -v $PWD:/conf \
        $CONTAINER_NAME generate-entity-gen-from-wikibase /conf/$BASE_ENTITIES_PY_FILE
fi

# And sync base_entities_gen.py
docker run \
--env-file $ENV_FILE \
-v $PWD/$BASE_ENTITIES_PY_FILE:/app/$BASE_ENTITIES_PY_FILE \
$CONTAINER_NAME sync-base-entities 


# Import CSV data
docker run \
--env-file $ENV_FILE \
-v $PWD/$BASE_ENTITIES_PY_FILE:/app/$BASE_ENTITIES_PY_FILE \
-v $DB_CSV_DIR:/data \
$CONTAINER_NAME import-csv-data --csv-files-dir /data $PROVIDER
