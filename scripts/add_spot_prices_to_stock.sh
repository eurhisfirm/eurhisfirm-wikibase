#!/bin/bash
#
# Update person functions for persons identified by IDs
#
CONTAINER_NAME=eurhisfirm-wikibase:latest
BASE_ENTITIES_PY_FILE=base_entities_gen.py

if [ $# -ne 3 ]; then
    echo "$0 <envfile> <spot_prices_csv_file> <dfih_stock_id>"
    exit 1
fi
ENV_FILE=$1
SPOT_PRICES_FILE=$2
DFIH_STOCK_ID=$3

if [ ! -f $ENV_FILE ]; then
    echo "Environnement file not found"
fi

if [ ! -f "$SPOT_PRICES_FILE" ]; then
    echo "spot prices csv file not found"
    exit 1
fi

# Gets base_entities_gen.py if it doesn't exist
if [ ! -f $PWD/$BASE_ENTITIES_PY_FILE ]; then
    docker run --env-file $ENV_FILE -v $PWD:/conf \
        $CONTAINER_NAME generate-entity-gen-from-wikibase /conf/$BASE_ENTITIES_PY_FILE
fi

# Update
docker run \
    --env-file $ENV_FILE \
    -v $PWD/$BASE_ENTITIES_PY_FILE:/app/$BASE_ENTITIES_PY_FILE \
    -v $SPOT_PRICES_FILE:/data/spot_prices.csv \
   $CONTAINER_NAME add-spot-prices-to-dfih-stock \
   --stock-spot-prices-csv /data/spot_prices.csv  \
   $DFIH_STOCK_ID
