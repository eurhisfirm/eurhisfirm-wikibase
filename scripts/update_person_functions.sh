#!/bin/bash
#
# Update person functions for persons identified by IDs
#
CONTAINER_NAME=eurhisfirm-wikibase:latest
BASE_ENTITIES_PY_FILE=base_entities_gen.py

if [ $# -ne 4 ]; then
    echo "$0 <envfile> <provider> <person_ids_file> <db_csv_dir>"
    exit 1
fi
ENV_FILE=$1
PROVIDER=$2
PERSON_IDS_FILE=$3
DB_CSV_DIR=$4

if [ ! -f $ENV_FILE ]; then
    echo "Environnement file not found"
fi

if [ "$PROVIDER" != "scob" -a "$PROVIDER" != "dfih" ]; then
    echo "Unknown provider: $PROVIDER, intended names are 'scob' and 'dfih'"
    exit 1
fi

if [ ! -f $PERSON_IDS_FILE ]; then
    echo "person ids file not found"
    exit 1
fi

if [ ! -d $DB_CSV_DIR ]; then
    echo "CSV dir not found"
    exit 1
fi

# Gets base_entities_gen.py if it doesn't exist
if [ ! -f $PWD/$BASE_ENTITIES_PY_FILE ]; then
    docker run --env-file $ENV_FILE -v $PWD:/conf \
        $CONTAINER_NAME generate-entity-gen-from-wikibase /conf/$BASE_ENTITIES_PY_FILE
fi

# Import CSV data
docker run \
    --env-file $ENV_FILE \
    -v $PWD/$BASE_ENTITIES_PY_FILE:/app/$BASE_ENTITIES_PY_FILE \
    -v $DB_CSV_DIR:/data \
    -v $PERSON_IDS_FILE:/dataconf/person_ids.csv \
   $CONTAINER_NAME update-person-functions \
   --person-ids-filepath /dataconf/person_ids.csv  \
   --csv-files-dir /data $PROVIDER
